%run a test CV analysis. 


%setup the input variables based on where you want the output and where you
%stored your input files
outputFile = 'adjusted_tournamentNb';
iodineFile = '/Users/crstock/Downloads/read_code_20170418/data/lists/list_iodine';
densityFile = '/Users/crstock/Downloads/read_code_20170418/data/lists/list_density';

%run the CV Analysis
%nbGen of 50000 is recommended for best performance
%adjust kFold to determine the number of cross validation iterations
%each optimization will stop early if it reaches the chi2target value
[params, outTrain, outTest, indices] = processgalist_crossval(outputFile, ...
    iodineFile, densityFile, 'nbGen', 2000, 'chi2scale', 2.903, ...
    'kFold', 4, 'chi2target', 278);
