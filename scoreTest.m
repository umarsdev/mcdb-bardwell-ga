%run the test trail from Scott and Loic

%run with:
%cd /Users/crstock/Documents/MATLAB/BardwellTest2/mcdb-bardwell-ga
%/Applications/MATLAB_R2016b.app/bin/matlab  -nodisplay -r scoreTest

%%
%test the standard process
outputFile = 'results/adjusted_tournamentNb';
iodineFile = '/Users/crstock/Downloads/read_code_20170418/data/lists/list_iodine';
densityFile = '/Users/crstock/Downloads/read_code_20170418/data/lists/list_density';


[params] = processgalist(outputFile, ...
    iodineFile, densityFile, 'nbGen', 2000, 'chi2scale', 2.903, ...
    'nTrials', 3, 'chi2target', 278);

%%
%test the iodo_dist setup
iodoFile = '/Users/srocchio/Desktop/test/iodine_list.txt';
outputFile = './results/trial.iod';
pdbFile = '/Users/srocchio/Desktop/test/list_pdb';
params = datasetup.iodo_dist(outputFile, iodoFile, pdbFile, 6500)