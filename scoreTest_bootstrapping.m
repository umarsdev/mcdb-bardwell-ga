%run the test trail from Scott and Loic

%run with:
%cd /Users/crstock/Documents/MATLAB/BardwellTest2/mcdb-bardwell-ga
%/Applications/MATLAB_R2016b.app/bin/matlab  -nodisplay -r scoreTest

outputFile = 'adjusted_tournamentNb';
iodineFile = '/Users/crstock/Downloads/read_code_20170418/data/lists/list_iodine';
densityFile = '/Users/crstock/Downloads/read_code_20170418/data/lists/list_density';

% outputFile = 'firstRun';
% iodineFile = '/home/crstock/bardwell277Data/lists/list_iodine';
% densityFile = '/home/crstock/bardwell277Data/lists/list_density';


[params, bootstat, bootsample, chi2mins] = processgalist_bootstrapping(outputFile, ...
    iodineFile, densityFile, 'nbGen', 2000, 'chi2scale', 2.903, ...
    'nTrials', 3, 'chi2target', 278);
