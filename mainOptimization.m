function params = mainOptimization(params, limitedInputs, iTrial, processingMode)

if processingMode == bwga.ProcessingMode.Bootstrap
    %limited inputs is an array of indexes available to the compos. We need to
    %modify abwga.ensemblealea so that it only selects from these available
    %values.
    iTrial = params.iTrialptr.Value + 1;
    params.iTrialptr.Value = iTrial;
end

%limitedInputs should be a row vector, but something in the bootstrapping
%messes that up.
if(~isrow(limitedInputs))
    limitedInputs = limitedInputs';
end

% Update the output filename for this parfor loop to append the trial
% number so that we aren't writing to the same file between parfor
% iterations.
params.outputFile = sprintf('%s-%d', params.outputFile, iTrial);

% As a test, try opening the .evol output file for writing before we
% proceed.  This should give us an indication if we have a permissions
% problem.
outputFileEvol = sprintf('%s.evol', params.outputFile);
fid = fopen(outputFileEvol, 'w');
assert(fid ~= -1, 'processgalist:ioError', ...
    'Cannot write to output file: %s', outputFileEvol);
%put header at the top of the file
fprintf(fid, 'Gen      chi2\n');
% Register a cleanup handler for the file.  This will close the file ID
% even if a ctrl-c keyboard sequence is detected.
fidCleanup = onCleanup(@() fclose(fid));

% Create a first generation with randomly generated ensembles.
nEnsembles = 4 * params.survivor;

for i = 1:nEnsembles
    params.gen.ens(i).compo = bwga.ensemblealea2(params.ensSize, limitedInputs);
end

params.nbTournament = params.tournamentNb(1);
params = bwga.selection(params);

for iGen = 0:params.nbGen
    if iGen > 0 && mod(iGen, params.tournamentChange) == 0
        params.tournament = min(params.tournament + 1, params.tournamentStep-1);
        params.nbTournament = params.tournamentNb(params.tournament);
    end

    params = bwga.genensemblealea(params);
    params = bwga.reproduction(params);
    params = bwga.mutation(params);
    params = bwga.selection(params);

    if mod(iGen, params.printFreq) == 0 || iGen == params.nbGen-1 || iGen == 0
        fprintf('Trial %d: %4d %10.3f\n', iTrial, iGen, params.chi2min);
        fprintf(fid, '%4d  %10.3f\n', iGen, params.chi2min);
    end

    % Check to see if we've reach the chi2target.  Exit the trial loop
    % early if we have.  This lets parfor go onto handle other trials
    % if we feel confident that the current one has hit its lower
    % bound.
    if params.chi2min <= params.chi2target
        fprintf('%% Trial %d reached chi2 target: %g\n', iTrial, params.chi2min);
        fprintf('Trial %d: %4d %10.3f\n', iTrial, iGen, params.chi2min);
        fprintf(fid, '%4d  %10.3f\n', iGen, params.chi2min);
        break;
    end
end

fprintf('%% Evol results written file: %s\n', outputFileEvol);

%% Write Output/Cleanup
bwga.writeoutput(params);
