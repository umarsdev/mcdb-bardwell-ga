function protein = ReadPdbFile_RXnoter(structName, structNb)
fAc(1:structNb) = 1000000;
lAc(1:structNb) = 0;

firstAmAcNb = 1000000;
lastAmAcNb = 0;

for s= 1:structNb
    fileName = structName{s};
    fid = fopen(fileName, 'r');
    assert(fid ~= -1, 'readlistfile:invalidFile', 'Could not open file: %s', fileName);
	contents = textscan(fid, '%4s %d %2s %4s %1s %d %f %f %f %s', 'Delimiter', '\n');
    for i = 1:length(contents{1})
        if strcmp(deblank(contents{3}(i)), 'CA') && strcmp(deblank(contents{1}(i)), 'ATOM')
            fAc(s) = min(fAc(s), contents{2}(i));
            lAc(s) = max(lAc(s), contents{2}(i));
        end
    end
    
    if (fAc(s) < 1000000 && lAc(s) > 0)
        firstAmAcNb = min(fAc(s), firstAmAcNb);
        lastAmAcNb = max(lAc(s), lastAmAcNb);
    end

    fclose(fid);
end

if (firstAmAcNb == 1000000 || firstAmAcNb == 0 || lastAmAcNb == 1000000 || lastAmAcNb == 0)
    sprintf('\n Problem reading the PDB structure\n')
end
protein = datasetup.InitProteinEAatom(firstAmAcNb, lastAmAcNb, structNb);

for s = 1:structNb
    fid = fopen(structName{s}, 'r');
    assert(fid ~= -1, 'ReadPdbFileRxnoter:structName', 'Could not open file: %s', structName{s});
	contents = textscan(fid, '%4s %d %2s %4s %1s %d %f %f %f %s', 'Delimiter', '\n');
    for i = 1:size(contents{2},1)
		atn = contents{2}(i);
        if strcmp(deblank(contents{3}(i)), 'CA') && ( atn~=1 && atn~=2 && atn~=12 && atn~=13 && atn~=14 && atn~=15 && atn~=25 && atn~=26) 
            protein(s).amAc(atn).resNb = contents{6}(i);
            protein(s).amAc(atn).ca(1) = contents{7}(i);
            protein(s).amAc(atn).ca(2) = contents{8}(i);
            protein(s).amAc(atn).ca(3) = contents{9}(i);
            protein(s).amAc(atn).caFlag = 1;
		else
            protein(s).amAc(atn).resNb = 0;
            protein(s).amAc(atn).ca(1) = 0;
            protein(s).amAc(atn).ca(2) = 0;
            protein(s).amAc(atn).ca(3) = 0;
            protein(s).amAc(atn).caFlag = 0;
        end
    end
    fclose(fid);
end
end
% 	while (!feof(fileIn)) {
%   //      amAcType_t          Res_Type;
% 	//    chainType_t          Chain_Type;
% 
% 		fscanf(fileIn, "%s %d %s %s %s %d %lf %lf %lf %[^\n] \n", at, &atn, att, res_name, chain_name, &resn, &x, &y, &z, end);
% 		if (atn >= firstAmAcNb && atn <= lastAmAcNb) {
% 			if (!strcmp(att, "CA")  && ( atn!=1 && atn!=2 && atn!=12 && atn!=13 && atn!=14 && atn!=15 && atn!=25 && atn!=26   ) ) {
% 				protein[s]->amAc[atn].resNb = resn;
% 			  //  AmAcName2Type(res_name, &Res_Type);
% 			  //  protein[s]->amAc[atn].amAcType = Res_Type;
% 			  //  ChainName2Type(chain_name, &Chain_Type);
% 			  //  protein[s]->amAc[atn].chainType = Chain_Type;
% 
% 				SET(protein[s]->amAc[atn].ca, 0, x);
% 				SET(protein[s]->amAc[atn].ca, 1, y);
% 				SET(protein[s]->amAc[atn].ca, 2, z);
% 				protein[s]->amAc[atn].caFlag = 1;
% 				//   fprintf(stdout,"prot %d res %d\n",s,resn);
% 			}
% 		}
% 	}
% 	fclose(fileIn);
% 	fileIn = NULL;
