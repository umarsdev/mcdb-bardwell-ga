function [fileList] =  MediaReadingPDB(mediaFileName)
%     FILE               *file;
%     char                fileName1[1000];
%     int                 m;
%     int                 mediaNb = 0;
fid = fopen(mediaFileName, 'r');
assert(fid ~= -1, 'MediaReadingPDB:invalidFile', 'Could not open file: %s', mediaFileName);
fidCleanup = onCleanup(@() fclose(fid));

% while (!feof(file)) {
%     fscanf(file, "%s\n", fileName1);
%     mediaNb++;
% }
% fclose(file);
% *pdbFileNames = malloc(mediaNb * sizeof **pdbFileNames);

% if ((file = fopen(mediaFileName, "r")) == NULL) {
%     printf("Impossible to open %s\n", "media");
%     exit(1);
% }

fileList = textscan(fid, '%s', 'Delimiter', '\n');
fileList = fileList{1};
% for (m = 0; m < mediaNb; m++) {
%     fscanf(file, "%s \n", fileName1);
%     (*pdbFileNames)[m] = malloc((strlen(fileName1) + 1) * sizeof *(*pdbFileNames)[m]);
%     strcpy((*pdbFileNames)[m], fileName1);
% }
end
% return mediaNb;
% }
