function data_iodo = ReadIodoFile(iodoFileName, m, data_iodo)
%     int                 num0, num;
%     double              x,y,z, sigma, S;
%     char                atom0[7],atom1[4], atom2[5],atom3[3], atom4[3];
%     FILE               *fileIn;
%     int                 i = 0;
fid = fopen(iodoFileName, 'r');
assert(fid ~= -1, 'readIodoFile:invalidFile', 'Could not open file: %s', iodoFileName);
fidCleanup = onCleanup(@() fclose(fid));
contents = textscan(fid, '%6s %d %1s %4s %1s %d %f %f %f %f %f %s', 'Delimiter', '\n');
for i = 1:length(contents{1})
    data_iodo.iodo(m).iodoPosition = contents{6}(i); %num
    data_iodo.iodo(m).iodo_xyz(i,1:3) = [contents{7}(i);contents{8}(i);contents{9}(i)] ; %x,y,z
    data_iodo.iodo(m).sigma(i) = contents{11}(i); %sigma
    data_iodo.iodo(m).flag(i) = 1;
    data_iodo.iodo(m).Outlier(i) = 1;
    data_iodo.iodo(m).iodoPosition2(i) = contents{6}(i); %num
end
data_iodo.iodo(m).totIodoNb = length(contents{1});
end