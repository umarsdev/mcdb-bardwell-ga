function i =  ReadIodoFileNb(iodoFileName)
%just get the number of lines in the file
fid = fopen(iodoFileName, 'r');
assert(fid ~= -1, 'ReadIodoFileNb:invalidFile', 'Could not open file: %s', iodoFileName);
% Register a cleanup to close the file when the function exits.
fidCleanup = onCleanup(@() fclose(fid));
contents = textscan(fid, '%s', 'Delimiter', '\n');

i = length(contents{1});
end