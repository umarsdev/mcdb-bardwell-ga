function params = IodoRan(outputFileName,params)
    iodoDist=params.iodoDist;
    iodoNoise=params.iodoNoise;
    
    iodoNb=params.data_iodo.iodoNb;
    %data_iodo_t                 *data_iodo = ((ensstructparam_t *)params)->data_iodo; %needs to stay as a pointer, so dont' chagne
    %int i=0,j=0;
    
    %FILE               *OutputFile;
    OutputFileName = sprintf('%s_ran.iod', outputFileName)
%     if ((OutputFile = fopen(OutputFileName, "w")) == NULL) {
%         printf("Impossible to open %s\n", OutputFileName);
%         exit(1);
%     }
    
    fid = fopen(OutputFileName, 'w');
    assert(fid ~= -1, 'IodoRan, failed to open output file', 'Could not open file: %s', OutputFileName);
    fidCleanup = onCleanup(@() fclose(fid));
    
    for j = 1:iodoNb
        totIodoNb = params.data_iodo.iodo(j).totIodoNb;
     
        for i = 1:totIodoNb
            
            num= params.data_iodo.iodo(j).iodoPosition2(i);
            sigma= params.data_iodo.iodo(j).sigma(i);
            %conf = ALEA(0,((ensstructparam_t *) params)->conformationNb); %ALEA is rand between first and second param
            conf = floor(rand * params.conformationNb);
%             gsl_vector    *pos    = gsl_vector_calloc(3);
%             gsl_vector    *vect   = gsl_vector_calloc(3);
%             gsl_vector    *u      = gsl_vector_calloc(3);
%             gsl_matrix    *ranrot = gsl_matrix_calloc(3,3);
            ca = params.protein(conf).amAc(num).ca;
            pos(3)=1;
            u(1) = rand; %ALEA(0,1));
            u(2) = rand; %SET(u,1,ALEA(0,1));
            u(3) = rand; %SET(u,2,ALEA(0,1));
            
            ranrot = datasetup.RandMat(u);
            vect = ranrot*pos';
            
            x = ca(1) + iodoDist * vect(1) + normrnd(0,iodoNoise) ;
            y = ca(2) + iodoDist * vect(2) + normrnd(0,iodoNoise) ;
            z = ca(3) + iodoDist * vect(3) + normrnd(0,iodoNoise) ;
            
            params.data_iodo.iodo(j).iodo_xyz(i,1)= x;
            params.data_iodo.iodo(j).iodo_xyz(i,2)= y;
            params.data_iodo.iodo(j).iodo_xyz(i,3)= z;
%             MSET(data_iodo->iodo[j].iodo_xyz, i , 1, y);
%             MSET(data_iodo->iodo[j].iodo_xyz, i , 2, z);     
            fprintf(fid, '%6s %5d %4s%1s%3s %1s%4d%1s   %8.3lf%8.3lf%8.3lf%6.2lf%6.2lf          %2s\n', 'HETATM', i, 'I', '', 'IOD', 'Z', num, '', x,y,z, sigma, sigma, 'I');
            
        end
    end
end