function [firstAmAcNb, lastAmAcNb] = RangePdbFile(fileName)
% FILE               *fileIn;
% 
% int                 atn, resn;
% 
% double              x, y, z;
% 
% char                at[7], att[5], res_name[5], chain_name[5], end[100];

fid = fopen(fileName, 'r');
assert(fid ~= -1, 'RangePdbFile:invalidFile', 'Could not open file: %s', fileName);
% Register a cleanup to close the file when the function exits.
fidCleanup = onCleanup(@() fclose(fid));

firstAmAcNb = 10000;
lastAmAcNb = 0;

contents = textscan(fid, '%4s %d %2s %4s %1s %d %f %f %f %s', 'Delimiter', '\n');
%contents is an array of cells. Each cell as the information for one of the variables
%contents{1} is all of the first column, {2} is the second
%access col 1, element 5 with contents{1}{5}
%careful: stirngs are in cells and numbers are not:
%string{a}{b} but number{a}(c)
for i = 1:size(contents{1},2)
    %A = sscanf(contents{i}, '%s %d %s %s %s %d %f %f %f %s');
    if strcmp(deblank(contents{3}(i)), 'CA') && strcmp(deblank(contents{1}(i)), 'ATOM')
        firstAmAcNb = min(firstAmAcNb, contents{6}(i));
        lastAmAcNb = max(lastAmAcNb, contents{6}(i));
    end
end
end
