function fileList = MediaReading1(mediaFileName)

fid = fopen(mediaFileName, 'r');
assert(fid ~= -1, 'MediaReading1:invalidFile', 'Could not open file: %s', mediaFileName);
fidCleanup = onCleanup(@() fclose(fid));
fileList = textscan(fid, '%s', 'Delimiter', '\n');
fileList = fileList{1};