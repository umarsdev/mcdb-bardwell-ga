from __future__ import division
from scitbx.array_family import flex
import time, os, sys
import iotbx.pdb
from iotbx import reflection_file_reader
from libtbx.test_utils import approx_equal
from cctbx import maptbx
from cctbx import miller

def compute_map(map_coefficients, crystal_gridding):
  fft_map = miller.fft_map(
    crystal_gridding     = crystal_gridding,
    fourier_coefficients = map_coefficients)
  fft_map.apply_volume_scaling()
  return fft_map.real_map_unpadded()
  
def map_score(xray_structure, map_data):
  result = 0
  sites_frac = xray_structure.sites_frac()
  weights    = xray_structure.atomic_weights()
  for sf, w in zip(sites_frac, weights):
    result += map_data.eight_point_interpolation(sf)*w
  return result/flex.sum(weights)

def run(use_cc_as_score=False, use_map_sum_as_score=True):
  assert [use_cc_as_score, use_map_sum_as_score].count(True) == 1
  # Read map coefficients; change data labels as needed
  mas = reflection_file_reader.any_reflection_file(file_name =
    "mapfile.mtz").as_miller_arrays()
  for ma in mas:
    if(ma.info().labels==['2FOFCWT_filled', 'PHI2FOFCWT_filled']):
      mc_2fofc = ma
  #  if(ma.info().labels==['FOFCWT', 'PHFOFCWT']):
  #    mc_fofc = ma
  cs1 = mc_2fofc.crystal_symmetry()
  # Define gridding for Fourier maps
  crystal_gridding = mc_2fofc.crystal_gridding(
    d_min             = mc_2fofc.d_min(),
    symmetry_flags    = maptbx.use_space_group_symmetry,
    resolution_factor = 1./3)
  # Compute Fourier maps
  map_2fofc = compute_map(
    map_coefficients=mc_2fofc, crystal_gridding=crystal_gridding)
#  map_fofc = compute_map(
#    map_coefficients=mc_fofc, crystal_gridding=crystal_gridding)
  # Read input model
  pdb_inp = iotbx.pdb.input(file_name="PDB_of_current_refined_structure.pdb")
  xrs = pdb_inp.xray_structure_simple()
  cs2 = xrs.crystal_symmetry()
  assert cs2.is_similar_symmetry(cs1)
  # Compute model map
 # f_calc = mc_2fofc.structure_factors_from_scatterers(
 #   xray_structure=xrs).f_calc()
 # map_fc = compute_map(
 #   map_coefficients=f_calc, crystal_gridding=crystal_gridding)
  # Compute map CC
#  cc = flex.linear_correlation(
#    x=map_2fofc.as_1d(), y=map_fc.as_1d()).coefficient()
 # print "Input model-map CC: %6.4f"%cc
 # print "Input model 2mFo-DFc map sum: %6.4f"%\
 #   map_score(xray_structure=xrs, map_data=map_2fofc)
 # print "Input model mFo-DFc map sum: %6.4f"%\
 #   map_score(xray_structure=xrs, map_data=map_fofc)
  #
  # Start looping over many models from MD
  #
  paths = [
    "Folder_Containing_PDBs/"]
  n_files = sum([len(os.listdir(path)) for path in paths])
  progress_scale = 100./n_files
  print "Total number of PDB files:", n_files
  cntr = 1
  log = sys.stdout
  for path in paths:
    file_names = os.listdir(path)
    print "PDB files in %s: %d"%(path, len(file_names))
    for file_name in file_names:
      partname = file_name.split()
      print partname[0]
      if(file_name.startswith("loopspep") and file_name.count(".pdb")==1):
        out = open("%s.log"%str(file_name),"w")
        #out = open("%s.log"%str(cntr), "w")
        pdb_inp = iotbx.pdb.input(file_name=path+file_name)
        xrs = pdb_inp.xray_structure_simple(crystal_symmetry = cs1)
        ms_2fofc = map_score(xray_structure=xrs, map_data=map_2fofc)
 #       ms_fofc  = map_score(xray_structure=xrs, map_data=map_fofc)
 #       print >> out, "Overall 2mFo-DFc value: %10.6f"%ms_2fofc
 #       print >> out, "Overall  mFo-DFc value: %10.6f"%ms_fofc
        for atom in pdb_inp.construct_hierarchy().atoms():
          site_frac = cs1.unit_cell().fractionalize(atom.xyz)
          m1 = map_2fofc.eight_point_interpolation(site_frac)
 #         m2 =  map_fofc.eight_point_interpolation(site_frac)
          print >> out, atom.format_atom_record()[:20], \
            "map: %8.4f"%(m1)
        log.write("\r%s %6.2f%%" %("Files processed so far:", 
          cntr*progress_scale))
        log.flush()
        cntr+=1
  
if(__name__ == "__main__"):
  t0 = time.time()
  run()
  print "Time: %6.4f"%(time.time()-t0)
