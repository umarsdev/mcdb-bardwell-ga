grep ATM 2ndrefineallsymmetryis.pdb | awk '{if(length($10)==4){print $0} else {print $1,$2,$3,$4,$5,$6,$7,$8,$9,substr($10,1,4),substr($10,5,length($10)-4),$11,$12,$13}}' | awk '{$11=sqrt($11/(8*3.14159*3.14159)) ; print $0}' > iodo_tmp

awk '{if($12=="I") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12}}' iodo_tmp > iodo.sym0

awk '{if($12=="AZZ0") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym1

awk '{if($12=="AZ00") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym2

awk '{if($12=="GZ00") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym3

awk '{if($12=="A0Z0") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym4

awk '{if($12=="G0Z0") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym5

awk '{if($12=="G000") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym6

awk '{if($12=="H000") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym7

awk '{if($12=="A0A0") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym8

awk '{if($12=="G0A0") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym9

awk '{if($12=="H0A0") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym10

awk '{if($12=="AA00") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym11

awk '{if($12=="GA00") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym12

awk '{if($12=="HA00") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym13

awk '{if($12=="AAA0") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym14

awk '{if($12=="HAA0") {printf "%6s %3d %1s %4s %1s %2d %8.3f %8.3f %8.3f %6.2f  %8.5f %1s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$13}}' iodo_tmp > iodo.sym15

rm iodo_tmp
