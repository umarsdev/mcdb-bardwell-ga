function params = density_ran(outputFileName, mapFileName, noiseavgX1k, noiselevelX1k)

outputFileName = bwga.fixFileName(outputFileName);
mapFileName = bwga.fixFileName(mapFileName);

params.noiseavg = noiseavgX1k / 1000;
params.noiselevel = noiselevelX1k / 1000;
%InitRandom();
%baseDir = fileparts(mapFileName);
map = datasetup.MediaReadingPDB(mapFileName);

sprintf('Noise Level     %8.3f \n', params.noiselevel);
sprintf('Average Density %8.3f \n', params.noiseavg);

%iterate over all of the map files and replace the y_1 element with noise

for m=1:length(map)
    %inFile = fullfile(baseDir, map{m});
    inFile = bwga.fixFileName(map{m}, mapFileName);
	fid = fopen(inFile, 'r');
    assert(fid ~= -1, 'density_ran:invalidFile', 'Could not open file: %s', inFile);

    resultsOutputFileName = sprintf('%s_%d.noise', outputFileName,m+1);

    fidOut = fopen(resultsOutputFileName, 'w');
    assert(fidOut ~= -1, 'density_ran:invalidFile', 'Could not open output file: %s', resultsOutputFileName);
    
    A = textscan(fid, '%6s %d %4s %6s %f', 'Delimiter', '\n');
    
    for i = 1:length(A{1})
        fprintf(fidOut, '%s %d %s %s %f\n', A{1}{i}, A{2}(i), A{3}{i}, A{4}{i}, params.noiseavg + normrnd(0,params.noiselevel));
    end
    fclose(fid);
    fclose(fidOut);

end
end