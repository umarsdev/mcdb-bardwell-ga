function protein = InitProtein_EAatom(firstAmAcNb, lastAmAcNb, conformationNb)

for c = 1:conformationNb
    protein(c).firstAmAcNb = firstAmAcNb;
    protein(c).lastAmAcNb = lastAmAcNb;

    %protein(c).amAc = malloc((lastAmAcNb + 2) * sizeof *(protein[c]->amAc));

    for i = 1:lastAmAcNb
        protein(c).amAc(i).resNb = i;
        protein(c).amAc(i).ca = [0; 0; 0];
        protein(c).amAc(i).caFlag = 0;

    end
    protein(c).amAc(lastAmAcNb + 1).resNb = -1;
end
end


