function params = WriteIodo_Back(outputFileName, params)

listFileName = sprintf('%s_iod.list',outputFileName)
listFid = fopen(listFileName, 'w');

for c=1:params.conformationNb
    rxOutputFileName = sprintf('%s_%d.iod',outputFileName ,c);
    fid = fopen(rxOutputFileName, 'w');
    assert(fid ~= -1, 'WriteIodo_Back:invalid file', 'Could not open file: %s', rxOutputFileName);
    datasetup.PrintIodoDistMin(fid ,params,  c) ;
    fclose(fid);
	fprintf(listFid, '%s\n', rxOutputFileName);
end
fclose(listFid);
rxOutputFileName = sprintf('%s_exp.iod', outputFileName);

fid = fopen(rxOutputFileName, 'w');
assert(fid ~= -1, 'WriteIodo_Back:invalidFile', 'Could not open file: %s', rxOutputFileName);

datasetup.PrintIodoDistMinTarget(fid,params, 0);
fclose(fid);

end
