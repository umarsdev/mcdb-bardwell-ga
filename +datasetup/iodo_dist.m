function params = iodo_dist(outputFileName, iodoFileName, pdbFileName, iodoDist)
%example run format:
%params = datasetup.iodo_dist('resuts/iodoPrep', '../../lists/list_iodine_data', '../../list_pdb', 6500)
%iodoDist input value is 1000x the actual value to match the c program input

%verify results with:
% iodoFile = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_iodine_data';
% outputFile = 'results/iodo_test';
% pdbFile = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_pdb';
% 

outputFileName = bwga.fixFileName(outputFileName, './');
pdbFileName = bwga.fixFileName(pdbFileName, './');
iodoFileName = bwga.fixFileName(iodoFileName, './');

params.iodoDist = iodoDist / 1000.;
sprintf('Iodine to CA Distance %f\n', params.iodoDist)
% baseDir = fileparts(iodoFileName);
iodo = datasetup.MediaReading1(iodoFileName);
% iodo = fullfile(baseDir, iodo);
[iodo, isAbsolute] = bwga.fixFileName(iodo, iodoFileName);
IodoNb = size(iodo, 1);
pdb = datasetup.MediaReadingPDB(pdbFileName);
% pdb = fullfile(baseDir, pdb);
pdb = bwga.fixFileName(pdb, pdbFileName);
structNb = length(pdb);

params.lastIodoNb= datasetup.ReadIodoFileNb(iodo{1});
params.firstIodoNb=1;
params.conformationNb = structNb;
params.dbSize = structNb;

params = DataInit(IodoNb, params, pdb, structNb);
params = datasetup.LectureFichiers( iodo,   params);
datasetup.WriteIodo_Back(outputFileName, params);

end

function params = DataInit(IodoNb, params, pdb, structNb)

[firstAmAcNb, lastAmAcNb] = datasetup.RangePdbFile(pdb{1});

params.firstAA=firstAmAcNb;
params.lastAA=lastAmAcNb;

params.data_iodo = datasetup.InitDataIodo(IodoNb, params.firstIodoNb, params.lastIodoNb);

params.protein = datasetup.ReadPdbFile_CA(pdb, structNb);
end
