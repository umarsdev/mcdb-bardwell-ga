function numList = ReadList(numberListFileName)

fid = fopen(numberListFileName, 'r');
assert(fid ~= -1, 'MediaReading1:invalidFile', 'Could not open file: %s', numberListFileName);
fidCleanup = onCleanup(@() fclose(fid));
numList = textscan(fid, '%d', 'Delimiter', '\n');
numList = numList{1};