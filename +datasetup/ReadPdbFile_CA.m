function protein = ReadPdbFile_CA(structName, structNb)
%from src/myLib.c
fAc(1:structNb) = 1000000;
lAc(1:structNb) = 0;
firstAmAcNb = 0;
lastAmAcNb = 1000000;

for s= 1:structNb
    fileName = structName{s};
    fid = fopen(fileName, 'r');
    assert(fid ~= -1, 'readlistfile:invalidFile', 'Could not open file: %s', fileName);
%     while (!feof(fileIn)) {
%         fscanf(fileIn, "%s %d %s %s %s %d %lf %lf %lf %[^\n] \n", at, &atn, att, res_name, chain_name, &resn, &x, &y, &z, end);
%         if (!strcmp(at, "ATOM") && !strcmp(att, "CA")) {
%             fAc[s] = MIN(fAc[s], resn);
%             lAc[s] = MAX(lAc[s], resn);
%         }
%     }
    
    contents = textscan(fid, '%4s %d %2s %4s %1s %d %f %f %f %s', 'Delimiter', '\n');
    for i = 1:length(contents{1})
        if strcmp(deblank(contents{3}(i)), 'CA') && strcmp(deblank(contents{1}(i)), 'ATOM')
            fAc(s) = min(fAc(s), contents{6}(i));
            lAc(s) = max(lAc(s), contents{6}(i));
        end
    end
    
    if (fAc(s) < 1000000 && lAc(s) > 0)
        firstAmAcNb = max(fAc(s), firstAmAcNb);
        lastAmAcNb = min(lAc(s), lastAmAcNb);
    end

    fclose(fid);
end
sprintf('Read:  %d %d\n', firstAmAcNb, lastAmAcNb);

if (firstAmAcNb == 1000000 || firstAmAcNb == 0 || lastAmAcNb == 1000000 || lastAmAcNb == 0)
    sprintf('\n Problem reading the PDB structure\n')
end

protein = datasetup.InitProteinEAatom(firstAmAcNb, lastAmAcNb, structNb);

for s = 1:structNb
    fid = fopen(structName{s}, 'r');
    assert(fid ~= -1, 'ReadPdbFile:structNames', 'Could not open file: %s', structName{s});

%     while (!feof(fileIn)) {
%         fscanf(fileIn, "%s %d %s %s %s %d %lf %lf %lf %[^\n] \n", at, &atn, att, res_name, chain_name, &resn, &x, &y, &z, end);
%         if (resn >= firstAmAcNb && resn <= lastAmAcNb) {
%             if ( !strcmp(att, "CA") ) {
%                 protein[s]->amAc[resn].resNb = resn;
%                 SET(protein[s]->amAc[resn].ca, 0, x);
%                 SET(protein[s]->amAc[resn].ca, 1, y);
%                 SET(protein[s]->amAc[resn].ca, 2, z);
%                 protein[s]->amAc[resn].caFlag = 1;
%              //   fprintf(stdout,"prot %d res %d\n",s,resn);
%             }
%         }
%     }
    contents = textscan(fid, '%4s %d %2s %4s %1s %d %f %f %f %s', 'Delimiter', '\n');
    for i = 1:size(contents{2},1)
        if strcmp(deblank(contents{3}(i)), 'CA') && strcmp(deblank(contents{1}(i)), 'ATOM')
            resn = contents{6}(i);
            protein(s).amAc(resn).resNb = contents{6}(i);
            protein(s).amAc(resn).ca(1) = contents{7}(i);
            protein(s).amAc(resn).ca(2) = contents{8}(i);
            protein(s).amAc(resn).ca(3) = contents{9}(i);
            protein(s).amAc(resn).caFlag = 1;
        else
            resn = contents{6}(i);
            protein(s).amAc(resn).resNb = 0;
            protein(s).amAc(resn).ca(1) = 0;
            protein(s).amAc(resn).ca(2) = 0;
            protein(s).amAc(resn).ca(3) = 0;
            protein(s).amAc(resn).caFlag = 0;
        end
    end
    fclose(fid);
end
end
