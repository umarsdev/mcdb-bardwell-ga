function PrintIodoDistMinTarget(outputFile, params, conf)
data_iodo = params.data_iodo;
num0=99999;
iodoDist=params.iodoDist;
j=0;
totIodoNb = data_iodo.iodo(1).totIodoNb;
for i = 1:totIodoNb
%     gsl_vector *iodo = gsl_vector_calloc(3);
    sigma = data_iodo.iodo(1).sigma;
    resInMin= params.data_iodo.iodo(1).iodoPosition2(i);
    num=resInMin;

    if (num==num0)
        j = j + 1;
    else 
        j = 1;
    end
    fprintf(outputFile,'%5d %5d %8.3f %8.3f\n',resInMin,j-1,iodoDist,  sigma(i));
    num0 = num;

end

end