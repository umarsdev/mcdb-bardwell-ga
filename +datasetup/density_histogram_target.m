function params = density_histogram_target(outputFileName, pdbFileName, mapFileName, listFileName, resX1k, densminX1k, densmaxX1k, flag, errorX1k)


outputFileName = bwga.fixFileName(outputFileName);
pdbFileName = bwga.fixFileName(pdbFileName);
mapFileName = bwga.fixFileName(mapFileName);

resolution = resX1k / 1000;
densmin = densminX1k /1000;
densmax = densmaxX1k / 1000;
err = errorX1k / 1000;
xxmax=0.;  xxmin=0.; xxor=0.;
yymax=0.;  yymin=0.; yyor=0.;
zzmax=0.;  zzmin=0.; zzor=0.;


%baseDir = fileparts(pdbFileName);
pdb = datasetup.MediaReadingPDB(pdbFileName);
map = datasetup.MediaReadingPDB(mapFileName);

%pdb = fullfile(baseDir, pdb);%make each element hold the full path
%map = fullfile(baseDir, map);

pdb = bwga.fixFileName(pdb, pdbFileName);
map = bwga.fixFileName(map, mapFileName);

structNb = length(pdb);
mapNb = length(map);
fprintf('Number of Structures     %8d\n', structNb)
fprintf('Binning Resolution       %8.3f\n', resolution)
fprintf( 'Minimal Electron Density %8.3f\n', densmin)
fprintf('Maximal Electron Density %8.3f\n', densmax)
fprintf('Electron Density Error   %8.3f\n', err)
fprintf('Print Conforming Values   %8d\n', flag)
assert(structNb == mapNb, 'struct and map files do not match');

params = DataInit(pdb, map, structNb);
params.conformationNb = structNb;

params.list = datasetup.ReadList(listFileName); %read a list of index numbers to look at

%Define the origin
count=0;
for i=1:structNb

	for r=params.firstAA:params.lastAA
		if (params.protein(i).amAc(r).caFlag==1)
			xxmax=max(xxmax,params.protein(i).amAc(r).ca(1));
			xxmin=min(xxmin,params.protein(i).amAc(r).ca(1));
			
			yymax=max(yymax,params.protein(i).amAc(r).ca(2));
			yymin=min(yymin,params.protein(i).amAc(r).ca(2));

			zzmax=max(zzmax,params.protein(i).amAc(r).ca(3));
			zzmin=min(zzmin,params.protein(i).amAc(r).ca(3));
			
			count = count + 1;
		end
	end
end
xxor =  xxmin;
yyor =  yymin;
zzor =  zzmin;
origin = [xxor; yyor; zzor];
%Define the size of the data

xxsteps = 1 + floor( (xxmax - xxmin) / resolution );
yysteps = 1 + floor( (yymax - yymin) / resolution );
zzsteps = 1 + floor( (zzmax - zzmin) / resolution );


fprintf('Histogram Origin      %8.3f %8.3f %8.3f\n', xxor, yyor, zzor);
fprintf('Histogram Lower Limit %8.3f %8.3f %8.3f\n', xxmin, yymin, zzmin);
fprintf('Histogram Upper Limit %8.3f %8.3f %8.3f\n', xxmax, yymax, zzmax);
fprintf('Histogram Dimension   %8d %8d %8d\n', xxsteps, yysteps, zzsteps);

%%%%%%%%%%%%%Compute experimental map
resultsOutputFileName = sprintf('%s_exp.map', outputFileName);

resultsOutputFile = fopen(resultsOutputFileName, 'w');
    assert(resultsOutputFile ~= -1, 'density_histogram:invalidFile', 'Could not open output file: %s', resultsOutputFileName);

count=0;

densSum = zeros(xxsteps,yysteps,zzsteps);
densCount = zeros(xxsteps,yysteps,zzsteps);

%iterate over all of the proteins and fill the two matricies: densSum and densCount
%for each one: calculate where it should be in the matrix and put its parts in.
for i=1:structNb
	%only process the ones that are on the list:
	if find(params.list == i-1)
		for r=params.firstAA:params.lastAA
			if (params.protein(i).amAc(r).caFlag==1)
				loc = params.protein(i).amAc(r).ca(1:3);
				%loc = loc + resolution/2; %shift so that we are finding centered on the indexes
				indi = round((loc-origin)/resolution);
				indi = indi+1; %can't have a zero index
				dens = max(params.protein(i).amAc(r).map_value,0);
				dens = min(dens,densmax);
				densSum(indi(1), indi(2), indi(3)) = densSum(indi(1), indi(2), indi(3)) + dens;
				densCount(indi(1), indi(2), indi(3)) = densCount(indi(1), indi(2), indi(3)) + 1;
			end
		end
	end
end
%the densSum and densCount are now full of all of the appropriate values;

densCalc = densSum./densCount;
densCalc(densCalc == inf) = 0; %from inf to 0
densCalc(isnan(densCalc)) = 0; %most values are nans from the conut of 0, so make them zero to use find.

ind = find(densCalc>densmin); %get the linear index to all of the interesting values

%now iterate over the values in ind and print to the file:
for i=1:length(ind)
	[a, b, c] = ind2sub(size(densCalc),ind(i));
	xx = xxor + resolution * (a-1);
	yy = yyor + resolution * (b-1);
	zz = zzor + resolution * (c-1);
	density  = densCalc(ind(i));
	fprintf(resultsOutputFile,'%6d  %8.3f %8.3f %8.3f   %9.7f %9.7f\n',ind(i), xx, yy, zz, density,err);
	%fprintf('%6d  %8.3f %8.3f %8.3f   %9.7f %9.7f\n',ind(i), xx, yy, zz, density,err);
end
fclose(resultsOutputFile);


%%%%%%%%%%Compute individual maps
if (flag==1) 

	for i = 1:structNb

		resultsOutputFileName = sprintf('%s_%d.map', outputFileName,i);

		resultsOutputFile = fopen(resultsOutputFileName, 'w');
		assert(resultsOutputFile ~= -1, 'density_histogram:invalidFile', 'Could not open output file: %s', resultsOutputFileName);
		densSum = zeros(xxsteps,yysteps,zzsteps);
		densCount = zeros(xxsteps,yysteps,zzsteps);

		for r=params.firstAA:params.lastAA
			if (params.protein(i).amAc(r).caFlag==1)
				loc = params.protein(i).amAc(r).ca(1:3);
				indi = floor((loc-origin)/resolution);
				indi = indi+1; %can't have a zero index
				dens = max(params.protein(i).amAc(r).map_value,0);
				dens = min(dens,densmax);
				densSum(indi(1), indi(2), indi(3)) = densSum(indi(1), indi(2), indi(3)) + dens;
				densCount(indi(1), indi(2), indi(3)) = densCount(indi(1), indi(2), indi(3)) + 1;
			end
		end
		%we now have all of the data setup in the matrix for this file. 
		%process it and write it.
		densCalc = densSum./densCount;
		densCalc(densCalc == inf) = 0; %from inf to 0
		densCalc(isnan(densCalc)) = 0; %most values are nans from the conut of 0, so make them zero to use find.
		densCalc(densCalc>densmax) = densmax; %limit to densmax
		ind = find(densCount); %get the linear index to all of the interesting values
		for k=1:length(ind)
			[a, b, c] = ind2sub(size(densCalc),ind(k));
			xx = xxor + resolution * (a-1);
			yy = yyor + resolution * (b-1);
			zz = zzor + resolution * (c-1);
			density  = densCalc(ind(k));
			fprintf(resultsOutputFile,'%6d  %8.3f %8.3f %8.3f   %9.7f\n',ind(k), xx, yy, zz, density);
		end
		fclose(resultsOutputFile);
	end
end

end

function params = DataInit(structName, mapName, structNb)
    %this one has its own RangePdbFile with a slightly different format
	%this one looks at the 2nd element (atn)
	params = RangePdbFile(structName{1});
    params.protein = datasetup.ReadPdbFile_RXnoter(structName, structNb);
    params.protein = datasetup.ReadRX(mapName, structNb, params.protein);
end

function params = RangePdbFile(fileName)
    
fid = fopen(fileName, 'r');
assert(fid ~= -1, 'RangePdbFile:invalidFile', 'Could not open file: %s', fileName);
% Register a cleanup to close the file when the function exits.
fidCleanup = onCleanup(@() fclose(fid));

firstAA = 10000;
lastAA = 0;

contents = textscan(fid, '%4s %d %2s %4s %1s %d %f %f %f %s', 'Delimiter', '\n');

for i = 1:length(contents{1})
    if strcmp(deblank(contents{3}(i)), 'CA') && strcmp(deblank(contents{1}(i)), 'ATOM')
        firstAA = min(firstAA, contents{2}(i));
        lastAA = max(lastAA, contents{2}(i));
    end
end
params.firstAA = firstAA;
params.lastAA = lastAA;
end
