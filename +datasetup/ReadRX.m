function protein = ReadRX(mapName, structNb, protein)
  
for s = 1:structNb
	fileName = mapName{s};
    fid = fopen(fileName, 'r');
    assert(fid ~= -1, 'ReadRX:invalidFile', 'Could not open file: %s', fileName);
	A = textscan(fid, '%6s %d %4s %6s %f', 'Delimiter', '\n');
	
	for i = 1:length(A{1})
		atn = A{2}(i);
		if atn >= protein(s).firstAmAcNb && atn <= protein(2).lastAmAcNb
			if protein(s).amAc(atn).caFlag == 1
				protein(s).amAc(atn).map_value = A{5}(i);
			end
		end
		
	end
	fclose(fid);
end

end

% 	
% 	while (!feof(fileIn)) {
%                                           1    2    3       4         5
% 		fscanf(fileIn, "%s %d %s %s %f\n", at, &atn, att, res_name, &map_value);
% 
% 		if (atn >= protein[s]->firstAmAcNb && atn <= protein[s]->lastAmAcNb) {
% 			if (protein[s]->amAc[atn].caFlag == 1) {
% 				protein[s]->amAc[atn].map_value = map_value;                        }
% 		}
% 	}
% 	fclose(fileIn);
% 	fileIn = NULL;
