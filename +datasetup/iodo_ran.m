function [params, iodo] = iodo_ran(outputFileName, iodoFileName, pdbFileName, iodoDist, iodoNoise)
rand('seed', 1);

outputFileName = bwga.fixFileName(outputFileName);
pdbFileName = bwga.fixFileName(pdbFileName);
iodoFileName = bwga.fixFileName(iodoFileName);

sprintf('iodine to CA distance %f', iodoDist)
%baseDir = fileparts(iodoFileName);
iodo = datasetup.MediaReading1(iodoFileName);
%iodo = fullfile(baseDir, iodo);
iodo = bwga.fixFileName(iodo, iodoFileName);
IodoNb = size(iodo, 1);
pdb = datasetup.MediaReadingPDB(pdbFileName);
% pdb = fullfile(baseDir, pdb);
pdb = bwga.fixFileName(pdb, pdbFileName);
structNb = length(pdb);
params.conformationNb = structNb;
params.dbSize = structNb;s
params.iodoDist = iodoDist;
params.iodoNoise = iodoNoise;
params = DataInit(IodoNb, params, pdb, iodo, structNb);
    
params = datasetup.LectureFichiers( iodo,   params);

params = datasetup.IodoRan(outputFileName,params);
    
%  WriteIodo_Back(outputFileName, params);

    
    %return 0;
end

function params = DataInit(IodoNb, params, pdb, iodo, structNb) 

[firstAmAcNb, lastAmAcNb] = datasetup.RangePdbFile(pdb{1});

params.firstAA=firstAmAcNb;
params.lastAA=lastAmAcNb;

params.firstIodoNb=1;
params.lastIodoNb = 0;

for i = 1:IodoNb
    params.lastIodoNb= max(params.lastIodoNb,datasetup.ReadIodoFileNb(iodo{i}));

end


params.data_iodo = datasetup.InitDataIodo(IodoNb, params.firstIodoNb, params.lastIodoNb);

params.protein = datasetup.ReadPdbFile_CA(pdb, structNb);

end