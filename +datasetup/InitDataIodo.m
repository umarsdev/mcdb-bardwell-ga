function data_iodo = InitDataIodo(iodoNb, firstIodoNb, lastIodoNb)
data_iodo.firstIodoNb = firstIodoNb;
data_iodo.lastIodoNb = lastIodoNb;
data_iodo.iodoNb = iodoNb;
%data_iodo->iodo = malloc(iodoNb * sizeof *data_iodo->iodo);
for m = 1:iodoNb

	data_iodo.iodo(m).Outlier = 1;
    data_iodo.iodo(m).sigma = 1e32;
    data_iodo.iodo(m).totIodoNb = 0;
    data_iodo.iodo(m).iodoPosition=0;
end

end
