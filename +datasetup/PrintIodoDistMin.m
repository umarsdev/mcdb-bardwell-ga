function params = PrintIodoDistMin(outputFile, params, conf)
    
num0=99999;
iodoDist=params.iodoDist;
    
totIodoNb = params.data_iodo.iodo(1).totIodoNb;
j = 1;
for i =1:totIodoNb
    sigma = params.data_iodo.iodo(1).sigma;
    distmin = 99999999999999.;
    bestconf=999999999;

    resInMin= params.data_iodo.iodo(1).iodoPosition2(i);
    num=resInMin;

    if (num==num0)
        j = j + 1;
    else
        j=1;
    end

    for c = 1:params.data_iodo.iodoNb
        iodo(1) = params.data_iodo.iodo(c).iodo_xyz(i,1);
        iodo(2) = params.data_iodo.iodo(c).iodo_xyz(i,2);
        iodo(3) = params.data_iodo.iodo(c).iodo_xyz(i,3);

        ca = params.protein(conf).amAc(resInMin).ca;

        dist=norm(iodo'-ca)-iodoDist;
        if abs(dist) < abs(distmin)
            bestconf=c;
            distmin=dist;
        end

    end
    fprintf(outputFile,'%5d %5d %8.3f %8.3f\n',resInMin,j-1,distmin+iodoDist, sigma(i));
    num0 = num;

end

end