%%test iodo_ran
outFile = './results/target';
iodineIn = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_iodine_target';
pdbIn = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_pdb_target';

datasetup.iodo_ran(outFile, iodineIn, pdbIn, 6500, 500)
%OK
%%
iodineIn = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_iodine_data';
pdbIn = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_pdb';
outFile = './results/target';
tic;
datasetup.iodo_dist(outFile, iodineIn, pdbIn, 6500)
a = toc
%Ok
%%
densityInput = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_density_data';
outFile = './results/target';
datasetup.density_ran(outFile, densityInput, 25, 100)
%OK
%%
outFile = './results/targetM';
pdbList = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_pdb';
densityData = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_density_data';
tic;
datasetup.density_histogram( outFile, pdbList, densityData, 4000, 50, 200, 100, 50, 1)
a = toc
%OK
%%
outFile = './results/targetT';
pdbList = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_pdb';
densityData = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_density_data_target';
listFileName = '/Users/crstock/Documents/MATLAB/BardwellTest2/READ_20170627/read_code_20170627/test/lists/list_target';
tic;
datasetup.density_histogram_target(outFile, pdbList, densityData, listFileName, 4000, 50, 200, 1, 50)
a = toc
%OK