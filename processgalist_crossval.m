function [params, outTrain, outTest, indices]  = processgalist_crossval(outputFile, iodineFile, densityFile, varargin)
% PROCESSGALIST_CROSSVAL  Processes iodine/density files using a genetic algorithm
%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% CrossValidation analysis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%this conducts a cross validation analysis based on the input parameter 'kFold'.
%1/kFold items are held out as the test set, and the remainder are used for
%training. 
%results are returned as the minimum chi2 values for each iteration in outTest and outTrain
%indices indicates which data were used for the test set on each iteration
%
%

% Syntax:
% params = PROCESSGALIST_CROSSVAL(outputFile, iodineFile, densityFile)
% params = PROCESSGALIST_CROSSVAL(outputFile, iodineFile, densityFile, options)
%
% Input:
% outputFile (1xN char) - The base name of the output file(s).  All output
%     files will have this base name plus a suffix.
% iodineFile (1xN char) - The name of the file containing the list of
%     iodine database files.
% densityFile (1xN char) - The name of the file containing the list of
%     density database files.
% options (key, value) - Set of key values that modify the analysis.
%     'ensSize', (number) - Default: 6
%     'survivor', (number) - Default: 100
%     'nbGen', (number) - Default: 50
%     'chi2scale', (number) - Default: 6.5
%     'chi2target', (number) - Default: NaN
%     'iodoFlag', (logical) - Default: true
%     'densityFlag', (logical) - Default: true
%     'seed', (number or string) - Default: 'shuffle'
%     'kFold', (number) - Default: 10
%     'parallel', (logical) - Default: true
%
% Example:
% outputFile = 'myOutput';
% iodineFile = 'data/lists/loopspep2_iodo'
% densityFile = 'data/lists/loopspep2_3Anoneg_noter_wc'
% params = PROCESSGALIST_CROSSVAL(outputFile, iodineFile, densityFile, ...
%                        'nbGen', 1000, 'chi2scale', 8, 'kFold', 10);

%% Parse Inputs

narginchk(1, Inf);

% Prior to parsing any inputs, we need to make sure that the installed
% version of MATLAB meets our minimum version of 2015b (8.6).
assert(~verLessThan('matlab', '8.6'), 'Minimum MATLAB version of 2015b required.');

p = inputParser;

% 'scalartext' wasn't added as an attribute for validateattributes until
% 2016b.
if verLessThan('matlab', '9.1')
    attributes = {'vector' 'nonempty'};
else
    attributes = {'scalartext' 'nonempty'};
end

% Output File
v = @(x) validateattributes(x, {'string' 'char'}, attributes, mfilename, ...
    'outputFile', 1);
addRequired(p, 'outputFile', v);

% List File
v = @(x) validateattributes(x, {'string' 'char'}, attributes, mfilename, ...
    'iodineFile', 2);
addRequired(p, 'iodineFile', v);

% Density File
v = @(x) validateattributes(x, {'string' 'char'}, attributes, mfilename, ...
    'densityFile', 3);
addRequired(p, 'densityFile', v);

% Defaults used in the optional parameters.
defaults.ensSize = 6;
defaults.survivor = 100;
defaults.nbGen = 50;
defaults.chi2scale = 6.5;
defaults.chi2target = NaN;
defaults.iodoFlag = true;
defaults.densityFlag = true;
defaults.seed = 'shuffle';
defaults.kFold = 10;
defaults.parallel = true;

% Optional parameters go here.  No validation checking at this time, but
% should be added in.  Since we're not doing any validation we'll do a lazy
% way of adding all the defaults to the inputParser object.
f = fieldnames(defaults);
for i = 1:length(f)
    addParameter(p, f{i}, defaults.(f{i}));
end


outputFile = bwga.fixFileName(outputFile);
iodineFile = bwga.fixFileName(iodineFile);
densityFile = bwga.fixFileName(densityFile);

parse(p, outputFile, iodineFile, densityFile, varargin{:});

% Copy the results of the inputParser into a params struct.  The p.Results
% field is read only and we plan on adding more parameters to params.
params = p.Results;

%% Setup

if params.parallel
    % Get a reference to the current parallel pool.  If one isn't running,
    % it'll be spun up here.
    parPool = gcp('nocreate');
    if isempty(parPool)
        fprintf('%% Parallel pool not running, starting it now.\n');
    end
    parPool = gcp;
end

% Setup our random number generator.
if params.parallel
    rng(params.seed, 'combRecursive');
else
    rng(params.seed, 'twister');
end

% *** DOCUMENT ***
% Param setup that Loic should comment to be more helpful to someone
% inspecting the code.

params.genSize = params.survivor * 6;
params.mutLevel = 5;

% Tournament parameters
params.tournament = 1;
params.tournamentNb = [max(params.survivor, 5), ...
                       max(params.survivor/2, 5), ...
                       max(params.survivor/4, 4), ...
                       max(params.survivor/10, 4), ...
                       max(params.survivor/20, 2), ...
                       max(params.survivor/50, 2), ...
                       max(params.survivor/100, 1), ...
                       max(params.survivor/200, 1)];
params.tournamentStep = length(params.tournamentNb);
params.tournamentChange = max(params.nbGen/params.tournamentStep, 1);

% Print frequency based on the number of generations with a minimum
% frequency of 1.
params.printFreq = max(ceil(params.nbGen/100), 1);

% Read the iodine files if toggled.
if params.iodoFlag
    % Get the list of iodine files to process.
    iodineList = bwga.readlistfile(params.iodineFile);
    
    % The total number of iodine files is the size of the list minus 1
    % because we don't include the metadata file.
    nIodineFiles = length(iodineList) - 1;
    
    % The files in the list are relative to the list filename, so we'll go
    % through the file list and make each entry a full file path so we
    % don't rely on relative directories.
%     p = fileparts(params.iodineFile);
%     iodineList = fullfile(p, iodineList);
	iodineList = bwga.fixFileName(iodineList, params.iodineFile);
end

% Read the density files if toggled.
if params.densityFlag
    % Get a list of the density files.
    densityList = bwga.readlistfile(params.densityFile);
    
    % The total number of density files is the size of the list minus 1
    % because we don't include the metadata file.
    nDensityFiles = length(densityList) - 1;
    
    % Make the relative file names full like we do in the section above.
%     p = fileparts(params.densityFile);
%     densityList = fullfile(p, densityList);
	densityList = bwga.fixFileName(densityList, params.densityFile);
end

% Set our database (file count) size depending on which flags were set.
if ~params.iodoFlag && params.densityFlag
    params.dbSize = nDensityFiles;
elseif params.iodoFlag && ~params.densityFlag
    params.dbSize = nIodineFiles;
elseif params.iodoFlag && params.densityFlag
    % Make sure the number of iodine/density files are the same.
    assert(nIodineFiles == nDensityFiles, 'processgalist:valueError', ...
        'Number of iodione and density files do not match: (%d,%d)', ...
        nIodineFiles, nDensityFiles);
    
    params.dbSize = nDensityFiles;
else
    error('Either/both iodoFlag and densityFlag must be set!');
end

% Read the iodine data if toggled.
if params.iodoFlag
    params = bwga.readiodinefiles(params, iodineList);
end

% Read the density data if toggled.
if params.densityFlag
    params = bwga.readdensityfiles(params, densityList);
end

% Create the gen structure and add it to params.
baseEns = struct('compo', zeros(1, params.ensSize), ...
                 'groupNb', 0, ...
                 'Chi2', 0, ...
                 'DeadorAlive', 0);
params.gen.ens = repmat(baseEns, 1, params.genSize);

% Put the fields in alphabetical order so it's easier to visually inspect
% the struct.
params = orderfields(params);

fprintf('\n================================================================');
fprintf('\n Database Size : %d', params.dbSize);
fprintf('\n Generation Size : %d', params.genSize);
fprintf('\n Ensemble Size : %d', params.ensSize);
fprintf('\n Survivor Number : %d', params.survivor);
fprintf('\n Number of Generations : %d', params.nbGen);
fprintf('\n Number of Iodo : %d', params.iodo_dist.iodoNb);
fprintf('\n Number of Density : %d', params.density.densityNb);
fprintf('\n Chi2 Scale : %f', params.chi2scale);
fprintf('\n================================================================\n');

%% Trials

% Record the processing start time.  At the end we'll spit out a total
% processing time to the command window.  This (obviously) doesn't include
% the time loading the data files.
tTrials = tic;

% If parallel is disabled, then set the number of parallel workers to 0
% which causes parfor to run like a regular for loop.
if params.parallel
    nWorkers = parPool.NumWorkers;
else
    nWorkers = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% CrossValidation analysis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%this conducts a cross validation analysis based on the input parameter 'kFold'.
%1/kFold items are held out as the test set, and the remainder are used for
%training. 
%results are returned as the minimum chi2 values for each iteration in outTest and outTrain


fullInputs = 1:params.dbSize;
%options = statset('UseParallel', 1);

indices=crossvalind('Kfold',fullInputs,params.kFold);
outTrain = [];
outTest = [];
parfor(i = 1:params.kFold, nWorkers)
    test = (indices == i); %tets gets 1/10th of the data
    train = ~test;
    %perform calculation with only the training set:
    trainData = fullInputs(train);
    testData = fullInputs(test);
    paramsOut = mainOptimization(params, trainData, i, bwga.ProcessingMode.Crossval);
    outTrain(i) = min(paramsOut.chi2min);
    paramsOut = mainOptimization(params, testData, i, bwga.ProcessingMode.Crossval);
    outTest(i) = min(paramsOut.chi2min);
end
fprintf('\n\naverage train value: %g\naverage test value: %g\n\n', mean(outTrain), mean(outTest));
fprintf('%% Total Processing Time: %g (s)\n', toc(tTrials));
