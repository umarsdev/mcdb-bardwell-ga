function [returnVal] = CHI2_RX(dist,sigma)
    partA = (dist/sigma)/2.9846;
    returnVal = 2.9846 * 2.9846 * (1.0 - exp(-1* (partA*partA)));
end