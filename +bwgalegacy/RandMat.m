function rotMat = RandMat(u)
    
theta = u(1) * 2. * pi;
phi = u(2) * 2. * pi;
z = u(3) * 2.;

r = sqrt(z);
Vx = sin(phi) * r;
Vy = cos(phi) * r;
Vz = sqrt(2.0 - z);


st = sin(theta);
ct = cos(theta);
Sx = Vx * ct - Vy * st;
Sy = Vx * st + Vy * ct;

% first Row
rotMat(1, 1) = Vx * Sx - ct;
rotMat(1, 2) = Vx * Sy - st;
rotMat(1, 3) = Vx * Vz;

% Second Row
rotMat(2, 1) = Vy * Sx + st;
rotMat(2, 2) = Vy * Sy - ct;
rotMat(2, 3) = Vy * Vz;

% Third Row
rotMat(3, 1) = Vz * Sx;
rotMat(3, 2) = Vz * Sy;
rotMat(3,3) = 1.0 - z;
end
