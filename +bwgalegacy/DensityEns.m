function [ymoy] = DensityEns(density, gen, ensSize, ensNum, densityIndex) 

d = density.elementDensity(densityIndex, gen.ens(ensNum).compo);
i = d > 0;

if any(i)
    % Doing it this is way is typically faster than using mean on
    % matrices that aren't huge, but your mileage may vary.
    ymoy = sum(d(i)) / sum(i);
    % ymoy = mean(d(i));
else
    ymoy = 0;
end
