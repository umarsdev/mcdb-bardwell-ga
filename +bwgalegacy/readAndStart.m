% DN = 1;
% DX = 2;
% DY = 3;
% DZ = 4;
% DV = 5;
% DE= 6;
% NB_DENSITY= 7;
% 
% IR = 1;
% IN = 2;
% IV = 3;
% IE = 4;
% NB_IODO_DIST= 5;
%params.ensSize = 6;
%params.survivor = 100;
%params.nbGen = 1000;
%params.chi2scale = 6.500;
%params.iodoFlag = 1;
%params.densityFalg = 1;
%readAndStart('output', '../lists/loopspep2_iodo', '../lists/loopspep2_3Anoneg_noter_wc', params )
function [params] = readAndStart( outputFileName, dataListFileName, densityFileName, params )
%This is the entry point for the whole program
%   structured based on the main function from read.c
    %assert(isa(outputFileName, 'string'));
    %assert(all(size(outputFileName) == [1 6]));
    %assert(isa(dataListFileName, 'string'));
    params.genSize = params.survivor * 6;
    params.mutLevel = 5;
    params.Tournament = 0;
    TournamentNb = zeros(8,1);
    TournamentNb(1) = max(params.survivor, 5);
    TournamentNb(2) = max(params.survivor/2, 5);
    TournamentNb(3) = max(params.survivor/4, 4);
    TournamentNb(4) = max(params.survivor/10, 4);
    TournamentNb(5) = max(params.survivor/20, 2);
    TournamentNb(6) = max(params.survivor/50, 2);
    TournamentNb(7) = max(params.survivor/100, 1);
    TournamentNb(8) = max(params.survivor/200, 1);
    rng('shuffle'); % seed randoms based on time
    % rng(1);

    if(params.iodoFlag == 1)
        [iodoConfNb, ioFileList] = DataReading(dataListFileName);
        iodoNb = IodoDistNb(string(ioFileList(1)));
        iodoConfNb = iodoConfNb - 1;
        %set values based on fullioList
        %iodo_dist.elementIodoDist = 
    end
    if(params.densityFlag == 1 )
        [densityConfNb, densityList] = DataReading(densityFileName);
        densityConfNb = densityConfNb(1) -1;
        densityNb = DensityNb(string(densityList(1)));
    end

    if(params.iodoFlag == 0 && params.densityFlag == 1)
        params.dbSize = densityConfNb;
    elseif(params.iodoFlag == 1 && params.densityFlag ==0)
        params.dbSize = iodoConfNb;
    elseif(params.iodoFlag == 1 && params.densityFlag ==1)
        if(densityConfNb ~= iodoConfNb)
            fprintf('missmatch between density and iodo');
            return;
        else
            params.dbSize=densityConfNb;
        end
    end
    %read input files and print the params before starting:
    params = ReadInputFiles(iodoNb, params.dbSize, densityNb, ioFileList, densityList, params);
    %print param summary:
    fprintf('\n=====================================================================================');
    fprintf('\n Database Size : %d', params.dbSize);
    fprintf('\n Generation Size : %d', params.genSize);
    fprintf('\n Ensemble Size : %d', params.ensSize);
    fprintf('\n Survivor Number : %d', params.survivor);
    fprintf('\n Number of Generations : %d', params.nbGen);
    fprintf('\n Number of Iodo : %d', iodoNb);
    fprintf('\n Number of Density : %d', densityNb);
    fprintf('\n Chi2 Scale : %f', params.chi2scale);
    fprintf('\n=====================================================================================\n');
    fprintf('setup time is %f\n', toc);
    %Perform the selection (evolution)
    [params] = Optimization(outputFileName, params, TournamentNb);
    WriteOutputFile(outputFileName, params);
    
    
end

function WriteOutputFile(outputFileName, params)
    
  %Prinf final generation
    ensOutputFileName = [ outputFileName '.ens'];
    ensOutputFile = fopen(ensOutputFileName, 'w');

    if(ensOutputFile == -1)
        fprintf('Impossible to output file: %s\n', ensOutputFileName);
        return
    end
    
    PrintGenFull(ensOutputFile, params);
    fclose(ensOutputFile);
   %Print back-calculated iodine data
 if params.iodoFlag==1
    
    iodoOutputFileName = [outputFileName '.iod'];
    iodoOutputFile = fopen(iodoOutputFileName, 'w');
    
    if (iodoOutputFile == -1)
        fprintf('Impossible to output file: %s\n', iodoOutputFileName);
        return
    end
    
    PrintIodoDistEns(iodoOutputFile, params, 1);
    fclose(iodoOutputFile);
 end
    %Print back-calculated density data
 if (params.densityFlag==1 )

    densOutputFileName  = [outputFileName '.den'];
    densOutputFile = fopen(densOutputFileName, 'w');
    
    if (densOutputFile == -1)
        fprintf('Impossible to output file: %s\n', densOutputFileName);
        return
    end

    PrintDensityEns(densOutputFile, params, 1);
    fclose(densOutputFile);
 end
end

function PrintDensityEns(OutputFile, params, ensNb)
    DN = 1;
    DX = 2;
    DY = 3;
    DZ = 4;
    DV = 5;
    DE= 6;
    density = params.density;
    gen = params.gen;
    ensSize = params.ensSize;
    densityNb = density.densityNb;
    
    fprintf(OutputFile, '%7s  %8s %8s %8s    %8s %8s %8s %8s\n', '#Number', 'x', 'y', 'z', 'Calc', 'Exp', 'Sigma', 'Chi2');
    for i = 1:densityNb 
        dens_exp = density.cibleDensity(i, DV);
        x = density.cibleDensity(i, DX);
        y = density.cibleDensity(i, DY);
        z = density.cibleDensity(i, DZ);
        nb = density.cibleDensity(i, DN);
        sigma = density.cibleDensity(i, DE);
        dens_calc= DensityEns(density, gen, ensSize, ensNb, i);
        chi2 = CHI2_HARM(dens_exp, dens_calc, sigma);

        if (sigma>0 && params.densityFlag==1 )
            fprintf(OutputFile, '%7d  %8.3f %8.3f %8.3f    %8.5f %8.5f %8.3f %8.3f\n', nb, x, y, z, dens_calc, dens_exp, sigma, chi2);
        end
    end
end


function PrintIodoDistEns(OutputFile, params, ensNb)
    IR = 1;
    IN = 2;
    IV = 3;
    IE = 4;
    iodo_dist = params.iodo_dist;
    gen = params.gen;
    ensSize = params.ensSize;

    %chi2 = 0.;
    iodoNb = iodo_dist.iodoNb;

    fprintf(OutputFile, '%7s %4s %4s  %8s %8s %8s %8s\n', '#Number', 'Res', 'Iodo', 'Calc', 'Exp', 'Sigma', 'Chi2');

    for i=1:iodoNb
        %double              iodo_exp,iodo_calc, sigma;
        %int                 nb, nr;

        iodo_exp = iodo_dist.cibleIodoDist(i, IV);
        nr = iodo_dist.cibleIodoDist(i, IR);
        nb = iodo_dist.cibleIodoDist(i, IN);
        sigma = iodo_dist.cibleIodoDist(i, IE);

        iodo_calc= IodoDistEns(iodo_dist, gen, ensSize, ensNb, i) ;
        chi2 = CHI2_RX( iodo_calc, sigma);

        if (sigma>0 && params.iodoFlag==1 )
              fprintf(OutputFile, '%7d %4d %4d  %8.3f %8.3f %8.3f %8.3f\n', i, nr, nb, iodo_calc + iodo_exp, iodo_exp, sigma, chi2);
        end
    end
end


function PrintGenFull(OutputFile, params)
    where = WhereInGen(params);
    fprintf(OutputFile, 'note we are subtracting 1 from indexes to match C based 0 index\nsur #  chi2      pdb#s\n');
    for (e = 1:where)
        fprintf(OutputFile, '%5d ', e);
        PrintEns(OutputFile, params, e);
    end

end

function PrintEns(OutputFile, params, ensNb)
    ensSize = params.ensSize;
    compo = params.gen.ens(ensNb).compo;

    fprintf(OutputFile, '%8.3f ', params.gen.ens(ensNb).Chi2);

    for e=1:ensSize
        fprintf(OutputFile, '%5d ', compo(e)-1);
    end
    fprintf(OutputFile, '\n');
end


function [ statusOut, tlines ] = DataReading(dataListFileName)
myFile = fopen(dataListFileName, 'r');
if (myFile == -1)
        fprintf('Impossible to open file in DataReading: %s\n', dataListFileName);
        statusOut = -1;
        return
end


tlines = cell(0,1);
tline = fgetl(myFile);
%rework to prevent 1 col of 0's
if(ischar(tline))
    tlines{1} = tline; %fill the first element specially
end
while 1
    tline = fgetl(myFile);
    if(ischar(tline))
        tlines{end+1,1} = tline; %#ok<AGROW>
    else
        break
    end
    
end

statusOut = size(tlines);
fclose(myFile);
end

function [countOut, outParams] = IodoDistNb(inFileName)
myFile = fopen(inFileName);
if(myFile == -1)
    fprintf('Impossible to open file in IodoDistNb: %s\n', inFileName);
    countOut = -1;
    return
end
formatSpec = '%d %d %f %f';
sizeA = [4 Inf];
outParams = fscanf(myFile, formatSpec, sizeA);
outParams = outParams';
countOut = size(outParams,1); 
fclose(myFile);
end

function [countOut, outParams] = DensityNb(inFileName)
myFile = fopen(inFileName);
if(myFile == -1)
    fprintf('Impossible to open file in DensityNb: %s\n', inFileName);
    countOut = -1;
    return
end
formatSpec = '%d %f %f %f %f %f';
sizeA = [6 Inf];
outParams = fscanf(myFile, formatSpec, sizeA);
outParams = outParams';
countOut = size(outParams,1); %note orientation is off on outParams so use outParams = outParams' if we want to do anything else with them
fclose(myFile);
end

function [newGen] = InitGen(genSize, ensSize)
for i = 1:genSize
    newGen.ens(i).compo = zeros(1, ensSize);
    newGen.ens(i).groupNb = 0;
    newGen.ens(i).Chi2 = 0;
    newGen.ens(i).DeadorAlive = 0;
end    
end

function [outDensity] = InitDataDensity(densityNb,dbSize)
    outDensity.densityNb = densityNb;
    %outDensity.cibleDensity = zeros(densityNb, densityType.NB_DENSITY);
    outDensity.elementDensity = -1*ones(densityNb, dbSize(1));
    %outDensity.elementDensity = spalloc(densityNb, dbSize(1), 300000); %trail data had 248438 elements
    %outDensity.elementDensity = sparse(densityNb, dbSize(1));
    outDensity.calcDensity = zeros(densityNb, 1);    
end

function [outDist] = InitDataIodoDist(iodoNb, dbSize)
    NB_IODO_DIST= 5; %defined at top of C file
    outDist.iodoNb = iodoNb;
    outDist.cibleIodoDist = zeros(iodoNb, NB_IODO_DIST);
    outDist.elementIodoDist = zeros(iodoNb, dbSize(1));
    outDist.calcIododist = zeros(iodoNb, 1);
end


function [params] = ReadInputFiles(IodoNb, structNb, densityNb, iodo, density, params)
    %Create appropriate structures
    params.gen = InitGen(params.genSize, params.ensSize);
    
    if (params.densityFlag==1 )
        params.density = InitDataDensity(densityNb,structNb);
    end
    
    if (params.iodoFlag==1  )
        iodo_dist1 = InitDataIodoDist(IodoNb, structNb);
    end
    %Read files
    %*** we are already reading the files when checking their values
    if (params.iodoFlag==1 )
        iodo_dist2 = ReadIodoDistTargetFile(string(iodo(1)));
        for m = 1:(params.dbSize)
            iodo_dist2 = ReadIodoDistElementFile(string(iodo(m+1)), iodo_dist2,m);
        end
    end
    
    %combine all of the iodo_dist elements back into params.iodo_dist
    if isfield(iodo_dist1, 'iodoNb')
        params.iodo_dist.iodoNb = iodo_dist1.iodoNb;
    end
    if isfield(iodo_dist2, 'elementIodoDist') %this has preference because it would be written last in the C version
        params.iodo_dist.elementIodoDist = iodo_dist2.elementIodoDist;
    elseif isfield(iodo_dist1, 'elementIodoDist')
        params.iodo_dist.elementIodoDist = iodo_dist1.elementIodoDist;
    end
    if isfield(iodo_dist2, 'cibleIodoDist')%this has preference because it is written last in the C version
        params.iodo_dist.cibleIodoDist = iodo_dist2.cibleIodoDist;
    elseif isfield(iodo_dist1, 'cibleIodoDist')
        params.iodo_dist.cibleIodoDist = iodo_dist1.cibleIodoDist;
    end
    if isfield(iodo_dist1, 'calcIododist')
        params.iodo_dist.calcIododist = iodo_dist1.calcIododist;
    end
    
    %save;
%     if (params.densityFlag==1 ) 
%         params.density = ReadDensityTargetFile(string(density(1)),  params.density) ;
%         for m = (1:params.dbSize-1)
%             params.density = ReadDensityElementFile(string(density(m+1)),  params.density,m);
%         end
%         params.density.elementDensity = full(params.density.elementDensity);
%     end

if params.densityFlag == 1
    params.density = ReadDensityTargetFile(density{1},  params.density);
    
    formatSpec = '%d %f %f %f %f';
    sizeA = [5 Inf];
    
    for iDB = 1:params.dbSize
        fid = fopen(density{iDB+1}, 'r');
        assert(fid ~= -1, 'Cannot open file: %s\n', density{iDB+1});
       
        outParams = fscanf(fid, formatSpec, sizeA);
        outParams = outParams';
        
        [lia, locb] = ismember(outParams(:, 1), params.density.cibleDensity(:,1));
        params.density.elementDensity(locb(lia), iDB) = outParams(lia,5);
        
        fclose(fid);
    end
end

    %nonZeros = nnz(params.density.elementDensity)
end

function [density] = ReadDensityElementFile(inFileName, density,structNb)
    %struckNb is 1 to num cols (10244)
   myFile = fopen(inFileName);
if(myFile == -1)
    fprintf('Impossible to open file: %s\n', inFileName);
    return
end
formatSpec = '%d %f %f %f %f';
sizeA = [5 Inf];
outParams = fscanf(myFile, formatSpec, sizeA);
outParams = outParams';
[rows, ~] = size(outParams);
cibleDensity = density.cibleDensity(:,1);
for i = 1:rows
%parfor i=1:rows
    [row, ~] = find(cibleDensity == outParams(i, 1));
    if row
        density.elementDensity(row, structNb) = outParams(i,5);
%         if(row>5 && structNb > 5)
%             fprintf('got a position of %d, %d\n', row, structNb);
%         end
%         if(row > 5 && structNb == 432)
%             fprintf('lets stop now');
%         end
%         thisTime = toc;
%         if thisTime > 0.006 %average should be 0.003 seconds
%             fprintf('had a time of %f\n', thisTime);
%         end
    end
end
fclose(myFile);
end

function [density] = ReadDensityTargetFile(inFileName, density)
myFile = fopen(inFileName);
if(myFile == -1)
    fprintf('Impossible to open file: %s\n', inFileName);
    return
end
formatSpec = '%d %f %f %f %f %f';
sizeA = [6 Inf];
outParams = fscanf(myFile, formatSpec, sizeA);
density.cibleDensity = outParams';

fclose(myFile);
end
        
function [iodo_dist] = ReadIodoDistElementFile(inFileName, iodo_dist,structNb)
myFile = fopen(inFileName);
if(myFile == -1)
    fprintf('Impossible to open file: %s\n', inFileName);
    return
end
formatSpec = '%d %d %f %f';
sizeA = [4 Inf];
outParams = fscanf(myFile, formatSpec, sizeA);
outParams = outParams';
numElements = size(outParams,1);
for i = 1:numElements
    iodo_dist.elementIodoDist(i, structNb) = outParams(i,3) - iodo_dist.cibleIodoDist(i, 3);
end
    
fclose(myFile);
end


function [iodo_dist] = ReadIodoDistTargetFile(inFileName)
myFile = fopen(inFileName);
if(myFile == -1)
    fprintf('Impossible to open file: %s\n', inFileName);
    return
end
formatSpec = '%d %d %f %f';
sizeA = [4 Inf];
outParams = fscanf(myFile, formatSpec, sizeA);
iodo_dist.cibleIodoDist = outParams'; %*** I transposed it!
%********** Do I need to flip the above matrix? check indexes used in other
%areas ***************************************************************
fclose(myFile);
end
