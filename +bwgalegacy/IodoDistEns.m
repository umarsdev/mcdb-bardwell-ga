function [y] = IodoDistEns(iodo_dist, gen, ensSize, ensNum, iodoIndex) 
    %iodo_dist's match. compo's dont' match but look reasonable
    y = 999999.;
    compo = gen.ens(ensNum).compo;
    elementIodoDist = iodo_dist.elementIodoDist;

    for e = 1:ensSize
        m = compo(e);
        y = min(y,abs(elementIodoDist(iodoIndex, m)));
    end
end