function [params] =  Optimization( outputFileName, params, TournamentNb )
%Take the full params and run the optimization sequence
TournamentStep = 8; %defined at top level of the C file. Should this be a param?

survivor = params.survivor;
nbGen = params.nbGen;
ensSize = params.ensSize;
dbSize = params.dbSize;
printFreq= max(ceil(nbGen/(100)),1);
TournamentChange= max(nbGen/TournamentStep,1);

evolOutputFileName = [outputFileName '.evol'];
myFile = fopen(evolOutputFileName, 'w');
if(myFile == -1)
    fprintf('Impossible to output file: %s\n', evolOutputFileName);
    return
end
%put header in the output file
fprintf(myFile, 'Gen      chi2\n');
%Create a first generation with only randomly generated ensemble and select the best ones
 
for k=1: 4 * survivor
    params.gen.ens(k).compo = EnsembleAlea(ensSize, dbSize);
end

params.nbTournament = TournamentNb(1);
params = Selection(params);
%Perform evolution (ramdom ensembles, reproductions and mutations) and select the best ones

for g = 0:nbGen

    if(g>0 && mod(g, TournamentChange) == 0 )
        params.Tournament = min(params.Tournament + 1,TournamentStep-1);
        params.nbTournament = TournamentNb(params.Tournament);
    end

        params = EnsembleAleaGen(params);
        params = Reproduction(params);
        params = Mutation(params);
        params = Selection(params);
    %print best chi2
    if(mod(g,printFreq) == 0 || g == nbGen -1 || g==0)
        fprintf('%4d %10.3f\n', g,params.chi2min);
        fprintf(myFile, '%4d  %10.3f\n', g, params.chi2min);
    end
end
fclose(myFile);
fprintf('evol results written to %s', outputFileName);

end

function [params] = Mutation(params)
	%int                 where, k;
	survivor = params.survivor;
    nbMut = 2*survivor;
    mutInterne = survivor;
	%ensSize = params.ensSize;
	%pseudoDbLenght = ensSize * survivor;
	%listsurvivor = ls_vector_int_calloc(pseudoDbLenght);
	%performance/speed note: should we pre-allocate listsuvivor?
	where = WhereInGen(params);
	[pseudoDbLenght, listsurvivor] = PseudoDb(params);
    for (k = 1:mutInterne)
        params = MutationInt(params,  k, where, listsurvivor, pseudoDbLenght);
    end
    for (k = (mutInterne + 1):nbMut)
        params = MutationExt(params,  k, where);
    end
end

function [params] = MutationInt(params,  ensNum, where, listsurvivor, pseudoDbLenght) 
      ensSize = params.ensSize;
      mutLevel =params.mutLevel;
      keepNb = floor((100 - mutLevel) * ensSize / 100);
      
      %*list = ls_vector_int_calloc(ensSize);

      compoOld = params.gen.ens(ensNum).compo;
      %*Mutant = ((ensstructparam_t *) params)->gen->ens[ensNum + where].compo;
      %replace Mutant with params.gen.ens(ensNum+where).compo
      %int                 e, k, i, out, val;

      list = randperm(ensSize);

      for e = 1:keepNb
            params.gen.ens(ensNum+where).compo(e) = compoOld(list(e));
      end

      e = keepNb;
      while (e < ensSize)
            i = randi([1, pseudoDbLenght], 1,1);%IALEA(0, pseudoDbLenght - 1);
            val = listsurvivor(i);
            out = 0;
            for (k = 1:e )
                  if (val == params.gen.ens(ensNum+where).compo(k)) 
                        out = 1;
                  end
            end
            if (out == 0)
                  params.gen.ens(ensNum+where).compo(e) = val;
                  e = e + 1;
            end
      end

      params.gen.ens(ensNum+where).compo = sort(params.gen.ens(ensNum+where).compo);

end

function [params] = MutationExt(params,  ensNum, where)
    ensSize = params.ensSize;
    dbSize = params.dbSize;
    mutLevel = params.mutLevel;
    keepNb = floor((100 - mutLevel) * ensSize / 100);
    %   /compoOld/params.gen.ens(ensNum).compo/
    %   /Mutant/params.gen.ens(ensNum+where).compo/
    
%       int                 ensSize = ((ensstructparam_t *) params)->ensSize;
%       int                 dbSize = ((ensstructparam_t *) params)->dbSize;
%       int                 mutLevel = ((ensstructparam_t *) params)->mutLevel;
%       int                 keepNb = (100 - mutLevel) * ensSize / 100;
	mutNb = ensSize - keepNb;
%       ls_vector_int     *list = ls_vector_int_calloc(ensSize);
%       ls_vector_int     *listdb = ls_vector_int_calloc(mutNb);
%       ls_vector_int     *compoOld = ((ensstructparam_t *) params)->gen->ens[ensNum].compo;
%       ls_vector_int     *Mutant = ((ensstructparam_t *) params)->gen->ens[ensNum + where].compo;
%       int                 e;

    %RandomList( list);
    list = randperm(ensSize);
    for e = 1:keepNb
      params.gen.ens(ensNum+where).compo(e) = params.gen.ens(ensNum).compo(list(e));
    end

    listdb = RandomListRestraint(mutNb, dbSize(1), params.gen.ens(ensNum+where).compo, ensSize);

    for e = 1:(ensSize-keepNb)
      params.gen.ens(ensNum+where).compo(e + keepNb) = listdb(e);
    end
    params.gen.ens(ensNum+where).compo = sort(params.gen.ens(ensNum+where).compo);
end

function [list] = RandomListRestraint(sizeList, valmax, restraint, sizeRest)
      %int                 i = 0, j = 0, k;
    j = 1;
    list = zeros(sizeList,1);
    while (j <= sizeList)
        out = 0;
        i = randi([1 valmax], 1, 1);%IALEA(0, valmax - 1);
        for (k = 1:sizeRest)
            if (i == restraint(k))
               out = 1;
            end
        end
        if (out == 0) 
              list(j) = i;
              j= j + 1;
        end
    end
end


function [length, listsurvivor] =  PseudoDb(params) 
    %ensSize = params.ensSize;
    survivor = params.survivor;
    %dbSize = params.dbSize;
    %int                 e, s;
    %length = survivor * ensSize;
    
    %speed/performance note: we are not pre-allocating
    %should be able to optimize this area some.
    listsurvivor = [params.gen.ens(1:survivor).compo];
    listsurvivor = unique(listsurvivor);
    length = size(listsurvivor,2);
%     for s = 1:survivor
%         compo = params.gen.ens(s).compo;
%         for e = 1:ensSize
%               listsurvivor((s-1) * ensSize + e) = compo(e);
%         end
%     end
%     listsurvivor = sort(listsurvivor);
% 
%     for (s = 1:survivor * ensSize - 1)
%         if (listsurvivor(s) == listsurvivor(s + 1))
%               listsurvivor(s) =  (dbSize(1) + 1);
%               length = length - 1;
%         end
%     end
%     listsurvivor = sort(listsurvivor);
end



function [params] =  Reproduction(params) %done
      %ensSize = params.ensSize;
      survivor = params.survivor;
      %int                 where;
      pairNb = survivor;
      %int                 parentlength;
      %int                 k;
      % *parent = ls_vector_int_calloc(2 * ensSize);
      % *pair = ls_matrix_int_calloc(pairNb, 2);
      %pair = zeros(pairNb, 2);
      where = WhereInGen(params);
      pair = SurvivorPair(params);

      for k = 1:pairNb
            %child = ((ensstructparam_t *) params)->gen->ens[k + where].compo;
            %replace child with: params.gen.ens(k+where).compo
            [parent, parentlength] = Parent(params, k, pair);
            
            %I was getting issues with parent size 1:11 but length of 12,
            %so I measure the parent length instead of using the returned
            %value above.

            params.gen.ens(k+where).compo = Child(params, parent, params.gen.ens(k+where).compo);
            %ls_vector_int_sort(params.gen.ens(k+where).compo);
            params.gen.ens(k+where).compo = sort(params.gen.ens(k+where).compo);
      end
end

function [child] = Child(params, parent, child)
    parentLength = size(parent,2);
    ensSize = params.ensSize;
    indexes = randperm(parentLength);
    child(1:ensSize) = parent(indexes(1:ensSize));
end


function [pair] = SurvivorPair(params)
    survivor = params.survivor;
    k = 1;
    pair = zeros(survivor,2);
    while (k <= survivor) 
        r1 = randi([1, survivor], 1, 1); %range 0 to survivor 1x1 matrix
        r2 = randi([1, survivor], 1, 1);
        if (r1 ~= r2)
              pair(k,2) = r1;
              pair(k,1) = r2;
              k = k+1;
        end
    end
end


function [parent, parentLength] = Parent(params, pairNum, pair)
    %set partent to compo from params.gen.ens[mum]
    
    ensSize = params.ensSize;
    %e, f, p = ensSize;
    p = ensSize;
    mum = pair(pairNum, 1);
    dad = pair(pairNum, 2);
    %set partent to compo from params.gen.ens[mum]
    parent(1:ensSize) = params.gen.ens(mum).compo(1:ensSize);
    %iterate through params.gen.ens[dad].compo(1:ensSize)
    %set parent[ensSize] to compo[e] any time a dad compo[e] has NO matches to any parent[f] element
        for e=1:ensSize
            compo = params.gen.ens(dad).compo;
            flag = 0;
            for f =1:ensSize
                  if parent(f) == compo(e)
                        flag = flag +1;
                  end
            end
            if flag == 0
                 parent(p) = compo(e);
                  p = p +1;
            end
        end
        parentLength = p;
end

function [params] =  EnsembleAleaGen(params)
    ensSize = params.ensSize;
    dbSize = params.dbSize;
    nbEnsAlea = params.survivor;

    where = WhereInGen(params);
    for k = where:where + nbEnsAlea
        %ls_vector_int     *compo = ((ensstructparam_t *) params)->gen->ens[k].compo;
        params.gen.ens(k).compo = EnsembleAlea(ensSize, dbSize);
    end
end

function [compo] = EnsembleAlea(ensSize, dbSize)
%generate a compo with a few random ints from 1:dbSize. dbSize is typically
%much larger than ensSize
compo(1:ensSize) = randperm(dbSize(1), ensSize);
end

function [params] = Selection(params) 

    params = RemoveDouble(params);
    lastEns = WhereInGen(params);
    params = TournamentOrganisation(params,  lastEns);
    params = Tournament(params, lastEns);
    params = Survivor(params);
end

function [params] = Survivor(params)
      genSize = params.genSize;
      ensSize = params.ensSize;
      survivor = params.survivor;
      for k = survivor:genSize
            params.gen.ens(k).compo = zeros(1,ensSize);
            params.gen.ens(k).groupNb = 0;
            params.gen.ens(k).Chi2 = 0.;
            params.gen.ens(k).DeadorAlive = 0;
      end
end


function [params] = Tournament(params, nbEns)
    %sort the ens elements by Chi2 and 
    nbTournament = params.nbTournament;
    survivor = params.survivor;
    %class = 0;
    survivorTour = max(survivor / nbTournament, 1);
    %index = zeros(1,nbEns);
    chi = zeros(1,nbEns);
    TournamentSurvivor = zeros(1,nbTournament);

    for k=1:nbEns
        if (k < survivor && params.gen.ens(k).Chi2 > 0)
              Chi2 = params.gen.ens(k).Chi2;
        else
              Chi2 = Chi2Ens(params, k);
              params.gen.ens(k).Chi2 = Chi2;
        end
        chi(k)=Chi2;
    end
    [~,index] = sort(chi); %get sort order and throw out sorted list
    params.chi2min = params.gen.ens(index(1)).Chi2;
    
    for k = 1:nbEns
        l = index(k);
        group = params.gen.ens(l).groupNb;
        params.gen.ens(l).DeadorAlive = 0;
        if (TournamentSurvivor(group) < survivorTour)
            params.gen.ens(l).DeadorAlive = 1;
            TournamentSurvivor(group) = TournamentSurvivor(group) + 1;
        end
    end

    k = 1;
    class = 1;
    %use the area beyond ens(nbEns) to store the best ranked elements
    while (k <= nbEns && class <= survivor)
        l = index(k);
        if (params.gen.ens(l).DeadorAlive == 1) 
              params.gen.ens(nbEns + class).compo = params.gen.ens(l).compo;
              params.gen.ens(nbEns + class).Chi2 = params.gen.ens(l).Chi2;
              class = class + 1;
        end
        k = k + 1;
    end
    %copy the best ranked elements to the first 1:survivor spots
    for k = 1:survivor
        params.gen.ens(k).compo = params.gen.ens(nbEns + k).compo;
        params.gen.ens(k).Chi2 = params.gen.ens(nbEns + k).Chi2;
    end
end

function [chi2] = chi2IodoDistEns(params, ensNum)
    IE = 4; %defiend in top level of C file
    iodo_dist = params.iodo_dist;
    gen = params.gen;

    ensSize = params.ensSize;
    iodoNb = iodo_dist.iodoNb;

    chi2 = 0;
    
    for i = 1:iodoNb 
        if iodo_dist.cibleIodoDist(i, IE) > 0
            partA = IodoDistEns(iodo_dist, gen, ensSize, ensNum, i);
            partB = iodo_dist.cibleIodoDist(i, IE);
            chi2 = chi2+ CHI2_RX(partA, partB);
        end
    end
end


function [chi2] = Chi2Ens(params, ensNum)
    chi2 = 0;
    if params.iodoFlag==1
        chi2 = chi2 + params.chi2scale * chi2IodoDistEns(params, ensNum);
    end

    if params.densityFlag==1
        chi2 = chi2 + chi2DensityEns(params, ensNum);
    end
end

function [chi2] = chi2DensityEns(params, ensNum)
DV = 5;
DE = 6;
density = params.density;
gen = params.gen;

ensSize = params.ensSize;
densityNb = density.densityNb;

assert(size(density.cibleDensity, 1) == densityNb);
assert(length(gen.ens(ensNum).compo) == ensSize);

i = density.cibleDensity(:,DE) > 0;
partA = density.cibleDensity(i,DV);
d = density.elementDensity(i,gen.ens(ensNum).compo);
d(d < 0) = NaN;
partB = nanmean(d, 2);
partB(isnan(partB)) = 0;
partC = density.cibleDensity(i, DE);
chi2 = sum(CHI2_HARM(partA, partB, partC));
end


function [params] = TournamentOrganisation(params, nbEns) 

      %nbTournament = ((ensstructparam_t *) params)->nbTournament;
      %corr = 0, n = 0;
      %list = ls_vector_int_calloc(nbEns);
      %int                 k;
      %RandomList( list);
      list = randperm(nbEns);
      corr = 0;
      n = 0;
      for k= 1:nbEns
            l = list(k);
            params.gen.ens(l).groupNb = k - n * params.nbTournament;
            corr = corr + 1;
            if corr == params.nbTournament
                  n = n + 1;
                  corr = 0;
            end
      end
end



function [params] = RemoveDouble(params)
nEns = length(params.gen.ens);

[~, ia] = unique(vertcat(params.gen.ens.compo), 'rows', 'stable');
nUniqueEns = length(ia);
params.gen.ens(1:nUniqueEns) = params.gen.ens(ia);

% Zero out the end of the structure.
if nUniqueEns < nEns
    params.gen.ens(nUniqueEns+1:end) = struct('compo', zeros(1, params.ensSize), ...
        'groupNb', 0, 'Chi2', 0, 'DeadorAlive', 0);
end
end
