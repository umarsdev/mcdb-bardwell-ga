function[runTime, params] =  entryPoint()
%run the optimization program:



%dirs based on running from:
%'/Users/crstock/Box Sync/Bardwell Lab/read_code/data/run'

%need to adjust
params.ensSize = 6;
params.survivor = 100;
params.nbGen = 1000;
params.chi2scale = 6.500;
params.iodoFlag = 1;
params.densityFlag = 1;
tic
params = readAndStart('output', '../lists/loopspep2_iodo', '../lists/loopspep2_3Anoneg_noter_wc', params )
runTime = toc

%outputFileName = 'output'
%dataListFileName ='../read_code/data/lists/loopspep2_iodo'
%densityFileName = '../read_code/data/lists/loopspep2_3Anoneg_noter_wc'
end