function IodoRan(outputFileName,params, r) 
iodoDist = params.iodoDist;
iodoNoise = params.iodoNoise;

iodoNb=params.data_iodo.iodoNb;
data_iodo = params.data_iodo;

sprintf(OutputFileName, '%s_ran.iod', outputFileName);
fid = fopen(OutputFileName, 'w');
assert(fid ~= -1, 'IodORan:failed to open ouput file', 'Could not open file: %s', OutputFileName);
fidCleanup = onCleanup(@() fclose(fid));

for j = 1:iodoNb
    totIodoNb = params.data_iodo.iodo(j).totIodoNb;

    for i = 1:totIodoNb

        num= data_iodo.iodo(j).iodoPosition2(i);
        sigma=data_iodo.iodo(j).sigma(i);
        %conf = ALEA(0,((ensstructparam_t *) params)->conformationNb);
        conf = rand * params.conformationNb;
%         double x,y,z;
%         gsl_vector    *pos    = gsl_vector_calloc(3);
%         gsl_vector    *vect   = gsl_vector_calloc(3);
%         gsl_vector    *u      = gsl_vector_calloc(3);
%         gsl_matrix    *ranrot = gsl_matrix_calloc(3,3);
        ca = params.protein(conf).amAc(num).ca;
        pos(2) = 1;
        u(0) = rand;
        u(1) = rand;
        u(2) = rand;


        RandMat(u, ranrot);
        MAT_VECT_PROD(ranrot,pos,vect);
        
        x = ca(0) + iodoDist * vect(0) + normrnd(r, iodoNoise);%GAUSS(iodoNoise) ;
        y = ca(1) + iodoDist * vect(1) + normrnd(r, iodoNoise);
        z = ca(2) + iodoDist * vect(2) + normrnd(r, iodoNoise);

        params.iodo(j).iodo_xyz(i , 0) = x;
        params.iodo(j).iodo_xyz(i , 1) = y;
        params.iodo(j).iodo_xyz(i , 2) = z;
        fprintf(fid, '%6s%5d %4s%1s%3s %1s%4d%1s   %8.3lf%8.3lf%8.3lf%6.2lf%6.2lf          %2s\n', 'HETATM', i, 'I', '', 'IOD', 'Z', num, '', x,y,z, sigma, sigma, 'I');
    end
end
fclose(fid);
end