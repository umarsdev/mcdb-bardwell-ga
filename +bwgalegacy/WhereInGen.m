function [lastEns] =  WhereInGen(params) 
lastEns = 1;
    for i=1:params.genSize
        if params.gen.ens(i).compo(1)==0 %if the first element is a zero then we need to stop
           %we got one that was zeros so return that index so return lastEns
           return;
        else %otherwise update lastEns
            lastEns = i; 
        end
    end
end
