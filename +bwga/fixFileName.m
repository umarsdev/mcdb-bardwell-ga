function [outName, isAbsolute] = fixFileName(inName, relativeSource)
%cleaning up file names. This function will let you use relative or absolute paths cleanly

%first lets remove the OS X space escaping and replace with a simple space
%'my\ file' will be changed to 'my file'
expression = '\\ ';
replace = ' ';
fixedSpaceName = regexprep(inName, expression, replace);

%now replace relative path assumption:
%now any file name that starts with a . will be considered to be a relative path from the lsit file
%Please don't start a file name with . if this isn't the behavior you want
if(iscell(inName))
	SW = startsWith(fixedSpaceName,'.');
	isAbsolute = ~SW;
	for i = 1:length(inName)
		if SW(i)
			pathstr = fileparts(relativeSource);
			outName{i} = fullfile(pathstr, fixedSpaceName{i});
        else
            outName{i} = fixedSpaceName{i};
        end
        
	end
else
	if fixedSpaceName(1) == '.'
		isAbsolute = 0;
		pathstr = fileparts(relativeSource);
		outName = fullfile(pathstr, fixedSpaceName);
	else
		isAbsolute = 1;
		outName = fixedSpaceName;
	end
end