function compo = ensemblealea(ensSize, dbSize)
% ENSEMBLEALEA
%
% Syntax:
%
% Description:
%
% Input:
%
% Output:
%

narginchk(2, 2);

assert(isscalar(ensSize) && isscalar(dbSize));

compo = sort(randperm(dbSize, ensSize));
