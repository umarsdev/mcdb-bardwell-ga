function params = genensemblealea(params)
% GENENSEMBLEALEA
%
% Syntax:
%
% Description:
%
% Input:
%
% Output:
%

narginchk(1, 1);
nargoutchk(1, 1);
    
s = bwga.whereingen(params);
e = s + params.survivor - 1;
for k = s:e
    params.gen.ens(k).compo = bwga.ensemblealea(params.ensSize, params.dbSize);
end
