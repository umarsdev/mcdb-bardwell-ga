function y = iododistens(iodo_dist, gen, ensSize, ensNum, iodoIndex)
% IODODISTENS
%
% Note:
% This function is more or less vestigial, but I'm leaving it here for now.
% There is an unnecessary amount of overhead just to find the minimum value
% of a small number of values as the ensemble size is typically 6.

y = min(abs(iodo_dist.elementIodoDist(iodoIndex, gen.ens(ensNum).compo)));
