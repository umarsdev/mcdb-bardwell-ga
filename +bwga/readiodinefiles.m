function params = readiodinefiles(params, iodineList)
% READIODINEFILES
%
%

narginchk(2, 2);

fprintf('%% Reading iodine data files...');

t0 = tic;

% Make sure the size of the database matches the numer of files we have
% minus the first file.
nFiles = length(iodineList) - 1;
assert(params.dbSize == nFiles, 'readiodinefiles:valueError', ...
    'DB size does not match number of iodine files: (%d,%d)', ...
    params.dbSize, nFiles);

% The first file in the list contains metadata, which we read out here.
sizeA = [4 Inf];
formatSpec = '%d %d %f %f';
params.iodo_dist.cibleIodoDist = bwga.readexpfile(iodineList{1}, formatSpec, sizeA);
params.iodo_dist.iodoNb = size(params.iodo_dist.cibleIodoDist, 1);

% Preallocate the memory to hold the element data.
params.iodo_dist.elementIodoDist = zeros(params.iodo_dist.iodoNb, params.dbSize);

% Iterate over the remaining files in the database.
for iDB = 1:params.dbSize
    fid = fopen(iodineList{iDB+1}, 'r');
    assert(fid ~= -1, 'Cannot open file: %s\n', iodineList{iDB+1});
    
    try
        % Extract the data.
        data = fscanf(fid, formatSpec, sizeA)';
        nElements = size(data, 1);
        
        % Not sure if this must be true, but assuming to always be true for
        % now so we'll enforce it as a sanity check.
        assert(nElements == params.iodo_dist.iodoNb);
        
        params.iodo_dist.elementIodoDist(:,iDB) = data(:,3) - params.iodo_dist.cibleIodoDist(:,3);
    catch e
        fclose(fid);
        rethrow(e);
    end
    
    fclose(fid);
end

params.iodo_dist.calcIododist = zeros(params.iodo_dist.iodoNb, 1);

t1 = toc(t0);
fprintf('Done %g(s)\n', t1);
