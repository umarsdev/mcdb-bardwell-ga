function params = readdensityfiles(params, densityList)
% READDENSITYFILES
%
%

narginchk(2, 2);

fprintf('%% Reading density data files...');

t0 = tic;

% Make sure the size of the database matches the numer of files we have
% minus the first file.
nFiles = length(densityList) - 1;
assert(params.dbSize == nFiles, 'readdensityfiles:valueError', ...
    'DB size does not match number of density files: (%d,%d)', ...
    params.dbSize, nFiles);

% The first file in the list contains metadata, which we read out here.
params.density.cibleDensity = bwga.readexpfile(densityList{1}, ...
    '%d %f %f %f %f %f', [6 Inf]);
params.density.densityNb = size(params.density.cibleDensity, 1);

% Preallocate memory
params.density.calcDensity = zeros(params.density.densityNb, 1);
params.density.elementDensity = -1*ones(params.density.densityNb, params.dbSize);

for iDB = 1:params.dbSize
    fid = fopen(densityList{iDB+1}, 'r');
    assert(fid ~= -1, 'Cannot open file: %s\n', densityList{iDB+1});
    
    try
        data = fscanf(fid, '%d %f %f %f %f', [5 Inf])';
        
        [lia, locb] = ismember(data(:,1), params.density.cibleDensity(:,1));
        params.density.elementDensity(locb(lia), iDB) = data(lia,5);
    catch e
        fclose(fid);
        rethrow(e);
    end
    
    fclose(fid);
end

t1 = toc(t0);
fprintf('Done %g(s)\n', t1);
