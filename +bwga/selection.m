function params = selection(params)
% SELECTION
%
% Syntax:
%
% Description:
%
% Input:
%
% Output:
%

narginchk(1, 1);
nargoutchk(1, 1);

%% Remove Duplicates

nEns = length(params.gen.ens);

[~, ia] = unique(vertcat(params.gen.ens.compo), 'rows', 'stable');
nUniqueEns = length(ia);
params.gen.ens(1:nUniqueEns) = params.gen.ens(ia);

% Zero out the end of the structure.
if nUniqueEns < nEns
    params.gen.ens(nUniqueEns+1:end) = struct('compo', zeros(1, params.ensSize), ...
        'groupNb', 0, 'Chi2', 0, 'DeadorAlive', 0);
end

%% Tournament Organization

% Find where the last generation is located.  Subtract off one because all
% the loop in this function ultimately need it to get the indexing right.
lastEns = bwga.whereingen(params) - 1;

list = randperm(lastEns);
corr = 0;
n = 0;
for k = 1:lastEns
    l = list(k);
    params.gen.ens(l).groupNb = k - n * params.nbTournament;
    corr = corr + 1;
    if corr == params.nbTournament
        n = n + 1;
        corr = 0;
    end
end

%% Tournament

survivorTour = max(params.survivor / params.nbTournament, 1);
chi = zeros(1, lastEns);
tournamentSurvivor = zeros(1, params.nbTournament);

for k = 1:lastEns
    if k <= params.survivor && params.gen.ens(k).Chi2 > 0
        chi2 = params.gen.ens(k).Chi2;
    else
        chi2 = chi2ens(params, k);
        params.gen.ens(k).Chi2 = chi2;
    end
    chi(k) = chi2;
end
[~, index] = sort(chi);
params.chi2min = min(chi);

for k = 1:lastEns
    l = index(k);
    group = params.gen.ens(l).groupNb;
    
    params.gen.ens(l).DeadorAlive = 0;
    if (tournamentSurvivor(group) < survivorTour)
        params.gen.ens(l).DeadorAlive = 1;
        tournamentSurvivor(group) = tournamentSurvivor(group) + 1;
    end
end

k = 1;
class = 1;
while k <= lastEns && class <= params.survivor
    l = index(k);
    if params.gen.ens(l).DeadorAlive == 1
        params.gen.ens(lastEns + class).compo = params.gen.ens(l).compo;
        params.gen.ens(lastEns + class).Chi2 = params.gen.ens(l).Chi2;
        class = class + 1;
    end
    k = k + 1;
end

for k = 1:params.survivor
    params.gen.ens(k).compo = params.gen.ens(lastEns + k).compo;
    params.gen.ens(k).Chi2 = params.gen.ens(lastEns + k).Chi2;
end

%% Survivor

for k = (params.survivor+1):params.genSize
    params.gen.ens(k).compo = zeros(1, params.ensSize);
    params.gen.ens(k).groupNb = 0;
    params.gen.ens(k).Chi2 = 0;
    params.gen.ens(k).DeadorAlive = 0;
end


function chi2 = chi2ens(params, ensNum)
chi2 = 0;

if params.iodoFlag
    chi2 = chi2 + params.chi2scale * chi2iododistens(params, ensNum);
end

if params.densityFlag
    chi2 = chi2 + chi2densityens(params, ensNum);
end


function chi2 = chi2densityens(params, ensNum)
DV = 5;
DE = 6;

% assert(size(params.density.cibleDensity, 1) == params.density.densityNb);
% assert(length(params.gen.ens(ensNum).compo) == params.ensSize);

i = params.density.cibleDensity(:,DE) > 0;
partA = params.density.cibleDensity(i,DV);
d = params.density.elementDensity(i,params.gen.ens(ensNum).compo);
d(d < 0) = NaN;
partB = mean(d, 2, 'omitnan');
partB(isnan(partB)) = 0;
partC = params.density.cibleDensity(i, DE);
chi2 = sum(bwga.chi2harm(partA, partB, partC));


function chi2 = chi2iododistens(params, ensNum)
% Defined in top level of C file.
IE = 4;

% chi2 = 0;
% 
% for i = 1:params.iodo_dist.iodoNb
%     if params.iodo_dist.cibleIodoDist(i, IE) > 0
%         % partA = bwga.iododistens(params.iodo_dist, params.gen, params.ensSize, ensNum, i);
%         partA = min(abs(params.iodo_dist.elementIodoDist(i, params.gen.ens(ensNum).compo)));
%         partB = params.iodo_dist.cibleIodoDist(i, IE);
%         chi2 = chi2 + bwga.chi2rx(partA, partB);
%     end
% end

i = params.iodo_dist.cibleIodoDist(:, IE) > 0;
dist = min(abs(params.iodo_dist.elementIodoDist(i, params.gen.ens(ensNum).compo)), [], 2);
chi2 = sum(bwga.chi2rx(dist, params.iodo_dist.cibleIodoDist(i, IE)));
