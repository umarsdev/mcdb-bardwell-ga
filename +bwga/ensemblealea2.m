function compo = ensemblealea2(ensSize, availableInputs)
%uses the available inputs as the only valid answers instead of the full
%range up to dbSize as in asemblealea()

narginchk(2, 2);

assert(isscalar(ensSize));

indexes = sort(randperm(max(size(availableInputs)), ensSize));
compo = availableInputs(indexes);

%compo must be a row vector to work.
if(~isrow(compo))
    compo = compo'
end

%compo = sort(randperm(dbSize, ensSize));
