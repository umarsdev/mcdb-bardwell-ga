classdef ProcessingMode
    enumeration
        Standard
        Crossval
        Bootstrap
    end
end
