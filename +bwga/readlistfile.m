function fileList = readlistfile(fileName)
% READLISTFILE  Reads a list file and returns its contents.
%
% Syntax:
% fileList = READLISTFILE(fileName)
%
% Description:
% Reads a list file which contains a dictionary of files to process and
% returns said list.
%
% Input:
% fileName (char array) - The name of the list file.
%
% Output:
% fileList (cell array) - List of files found in the list file.  Each element
%     of the cell array is a char array.

narginchk(1, 1);

fid = fopen(fileName, 'r');
assert(fid ~= -1, 'readlistfile:invalidFile', 'Could not open file: %s', fileName);

% Register a cleanup to close the file when the function exits.
fidCleanup = onCleanup(@() fclose(fid));

fileList = textscan(fid, '%s', 'Delimiter', '\n');
fileList = fileList{1};
