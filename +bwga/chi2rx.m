function returnVal = chi2rx(dist, sigma)
% CHI2RX
%
%

narginchk(2, 2);

partA = (dist./sigma) / 2.9846;
returnVal = 2.9846^2 * (1 - exp(-1 * partA.^2));
