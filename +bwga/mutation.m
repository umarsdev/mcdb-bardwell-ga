function params = mutation(params)

narginchk(1, 1);
nargoutchk(1, 1);

nbMut = 2*params.survivor;
mutInterne = params.survivor;
where = bwga.whereingen(params) - 1;

% PseudoDb
listsurvivor = [params.gen.ens(1:params.survivor).compo];
listsurvivor = unique(listsurvivor);
pseudoDbLength = size(listsurvivor, 2);

for k = 1:mutInterne
    params = mutationint(params, k, where, listsurvivor, pseudoDbLength);
end

for k = (mutInterne + 1):nbMut
    params = mutationext(params, k, where);
end


function params = mutationext(params,  ensNum, where)

keepNb = floor((100 - params.mutLevel) * params.ensSize / 100);
mutNb = params.ensSize - keepNb;
iEns = ensNum + where;

list = randperm(params.ensSize);
for e = 1:keepNb
    params.gen.ens(iEns).compo(e) = params.gen.ens(ensNum).compo(list(e));
end

listdb = randomlistrestraint(mutNb, params.dbSize, params.gen.ens(iEns).compo, ...
    params.ensSize);

for e = 1:(params.ensSize-keepNb)
    params.gen.ens(iEns).compo(e + keepNb) = listdb(e);
end

% % This is a closer representation of how Loic's C code was structured,
% % but the above bit does the same thing.
% for e = (keepNb+1):params.ensSize
%     params.gen.ens(iEns).compo(e) = listdb(e - keepNb);
% end

params.gen.ens(iEns).compo = sort(params.gen.ens(iEns).compo);


function params = mutationint(params,  ensNum, where, listsurvivor, pseudoDbLength)

keepNb = floor((100 - params.mutLevel) * params.ensSize / 100);

compoOld = params.gen.ens(ensNum).compo;
list = randperm(params.ensSize);
iEns = ensNum + where;

for e = 1:keepNb
    params.gen.ens(iEns).compo(e) = compoOld(list(e));
end

e = keepNb + 1;
while e <= params.ensSize
    i = randi([1, pseudoDbLength], 1, 1);
    val = listsurvivor(i);
    out = 0;
    for k = 1:e
        if val == params.gen.ens(iEns).compo(k)
            out = 1;
        end
    end
    if out == 0
        params.gen.ens(iEns).compo(e) = val;
        e = e + 1;
    end
end

params.gen.ens(iEns).compo = sort(params.gen.ens(iEns).compo);


function list = randomlistrestraint(sizeList, valmax, restraint, sizeRest)

j = 1;
list = zeros(sizeList,1);

while j <= sizeList
    out = 0;
    i = randi([1 valmax], 1, 1);
    
    for k = 1:sizeRest
        if i == restraint(k)
            out = 1;
        end
    end
    if out == 0
        list(j) = i;
        j= j + 1;
    end
end
