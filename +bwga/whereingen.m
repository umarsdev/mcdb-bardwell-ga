function lastEns = whereingen(params, searchMethod)
% WHEREINGEN  Returns the first empty compo in the gen structure.
%
% Syntax:
% lastEns = whereingen(params)
% lastEns = WHEREINGEN(params, searchMethod)
%
% Description:
% Returns the first empty compo array in the gen structure, i.e. contains
% all zeros.  This value will be 1 index past the last valid compo, so take
% this into consideration when using it.
%
% Input:
% params (struct) - Experimental params struct containing the 'gen'
%     structure in which the ens.compo variable(s) exist.
% searchMethod (scalar number) - 0 for a C style loop search, 1 for a more
%     memory inefficient but likely faster MATLAB search.  Default: 0
%
% Output:
% lastEns (scalar number) - The index of the first empty gen.ens.compo
%     array.  If not empty arrays are found, this will be set to
%     params.genSize + 1.

narginchk(1, 2);

if nargin == 1
    searchMethod = 0;
end

% We have a couple options for the search method.  Default to method 0
% 0 - Uses the least memory but loops over all elements.
% 1 - Uses more memory, but in theory lets MATLAB quickly search through all
%    values of the compo data.
switch searchMethod
    case 0
        for lastEns = 1:params.genSize
            % Stop when the first element of compo is zero.
            if params.gen.ens(lastEns).compo(1) == 0
                break;
            end
        end
        
    case 1
        compo = vertcat(params.gen.ens.compo);
        lastEns = find(compo(:,1) == 0, 1);
        
        if isempty(lastEns)
            lastEns = params.genSize + 1;
        end
        
    otherwise
        error('whereingen:inputError', 'Unknown search method: %g\n', ...
            searchMethod);
end

% As a sanity check make sure the row that was found contains all zeros.
% This should never happen if we're doing everything right.
if lastEns <= params.genSize
    assert(all(params.gen.ens(lastEns).compo == 0), ...
        'whereingen:valueError', ...
        'Not all values are zero for lastEns of %d', lastEns);
end
