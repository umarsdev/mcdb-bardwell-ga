function retVal = chi2harm(calc, mes, sigma)     

% narginchk(3, 3);
% 
% % All inputs should be vectors.
% assert(isvector(calc) && isvector(mes) && isvector(sigma), ...
%     'CHI2_HARM:inputError', 'Inputs must be vectors.');
% 
% % Make sure everything is the same size.
% assert(isequal(size(calc), size(mes), size(sigma)), ...
%     'CHI2_HARM:inputError', 'Inputs must be the same size.');

retVal = ((calc - mes) ./ sigma) .^ 2;
