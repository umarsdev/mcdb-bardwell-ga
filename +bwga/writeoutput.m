function writeoutput(params)

% Open the .ens file.
ensOutputFileName = [params.outputFile '.ens'];
ensFid = fopen(ensOutputFileName, 'w');
assert(ensFid ~= -1, 'writeoutput:ioError', ...
    'Cannot write to file: %s', ensOutputFileName);
ensCleanup = onCleanup(@() fclose(ensFid));

% Print the gen info.
printgenfull(ensFid, params);

% Print back-calculated iodine data.
if params.iodoFlag
    iodoOutputFileName = [params.outputFile '.iod'];
    iodFid = fopen(iodoOutputFileName, 'w');
    assert(iodFid ~= -1, 'writeoutput:ioError', ...
        'Cannot write to file: %s', iodoOutputFileName);
    iodCleanup = onCleanup(@() fclose(iodFid));
    
    printiododistens(iodFid, params, 1);
end

% Print back-calculated density data.
if params.densityFlag
    densOutputFileName  = [params.outputFile '.den'];
    densFid = fopen(densOutputFileName, 'w');
    assert(densFid ~= -1, 'writeoutput:ioError', ...
        'Cannot write to file: %s', densOutputFileName);
    densCleanup = onCleanup(@() fclose(densFid));
    
    printdensityens(densFid, params, 1);
end


function printdensityens(fid, params, ensNb)
DN = 1;
DX = 2;
DY = 3;
DZ = 4;
DV = 5;
DE= 6;
density = params.density;
gen = params.gen;
ensSize = params.ensSize;
densityNb = density.densityNb;

fprintf(fid, '%7s  %8s %8s %8s    %8s %8s %8s %8s\n', '#Number', 'x', 'y', ...
    'z', 'Calc', 'Exp', 'Sigma', 'Chi2');

for i = 1:densityNb
    dens_exp = density.cibleDensity(i, DV);
    x = density.cibleDensity(i, DX);
    y = density.cibleDensity(i, DY);
    z = density.cibleDensity(i, DZ);
    nb = density.cibleDensity(i, DN);
    sigma = density.cibleDensity(i, DE);
    dens_calc = bwga.densityens(density, gen, ensSize, ensNb, i);
    chi2 = bwga.chi2harm(dens_exp, dens_calc, sigma);
    
    if sigma > 0 && params.densityFlag == 1
        fprintf(fid, '%7d  %8.3f %8.3f %8.3f    %8.5f %8.5f %8.3f %8.3f\n', ...
            nb, x, y, z, dens_calc, dens_exp, sigma, chi2);
    end
end


function printiododistens(fid, params, ensNb)
IR = 1;
IN = 2;
IV = 3;
IE = 4;
iodo_dist = params.iodo_dist;

iodoNb = iodo_dist.iodoNb;

fprintf(fid, '%7s %4s %4s  %8s %8s %8s %8s\n', '#Number', 'Res', 'Iodo', ...
    'Calc', 'Exp', 'Sigma', 'Chi2');

for i = 1:iodoNb
    iodo_exp = iodo_dist.cibleIodoDist(i, IV);
    nr = iodo_dist.cibleIodoDist(i, IR);
    nb = iodo_dist.cibleIodoDist(i, IN);
    sigma = iodo_dist.cibleIodoDist(i, IE);
    
    % iodo_calc = bwga.iododistens(iodo_dist, gen, ensSize, ensNb, i);
    iodo_calc = min(abs(params.iodo_dist.elementIodoDist(i, params.gen.ens(ensNb).compo)));
    chi2 = bwga.chi2rx(iodo_calc, sigma);
    
    if sigma > 0 && params.iodoFlag == 1
        fprintf(fid, '%7d %4d %4d  %8.3f %8.3f %8.3f %8.3f\n', i, nr, nb, iodo_calc + iodo_exp, iodo_exp, sigma, chi2);
    end
end


function printgenfull(fid, params)
where = bwga.whereingen(params) - 1;

fprintf(fid, 'note we are subtracting 1 from indexes to match C based 0 index\nsur #  chi2      pdb#s\n');

for e = 1:where
    fprintf(fid, '%5d ', e);
    
    printens(fid, params, e);
end


function printens(fid, params, ensNb)
ensSize = params.ensSize;
compo = params.gen.ens(ensNb).compo;

fprintf(fid, '%8.3f ', params.gen.ens(ensNb).Chi2);

for e = 1:ensSize
    fprintf(fid, '%5d ', compo(e)-1);
end

fprintf(fid, '\n');
