function exp = readexpfile(fileName, formatSpec, sizeA)
% READEXPFILE
%
% Syntax:
%
% Description:
%
% Input:
%
% Output:

narginchk(3, 3);

fid = fopen(fileName, 'r');
assert(fid ~= -1, 'readexpfile:invalidFile', 'Could not open file: %s', fileName);

% Register a cleanup to close the file when the function exits.
fidCleanup = onCleanup(@() fclose(fid));

exp = fscanf(fid, formatSpec, sizeA)';
