function params = reproduction(params)
% REPRODUCTION
%
% Syntax:
%
% Description:
%
% Input:
%
% Output:
%

narginchk(1, 1);
nargoutchk(1, 1);

where = bwga.whereingen(params) - 1;

% Create the survivor pairs.
k = 1;
pair = zeros(params.survivor, 2);
while k <= params.survivor
    r1 = randi([1, params.survivor], 1, 1); %range 0 to survivor 1x1 matrix
    r2 = randi([1, params.survivor], 1, 1);
    if r1 ~= r2
        pair(k,1) = r1;
        pair(k,2) = r2;
        k = k+1;
    end
end

for k = 1:params.survivor
    %% Parent
    
    p = params.ensSize + 1;
    mum = pair(k, 1);
    dad = pair(k, 2);
    
    % Set parent to compo from params.gen.ens[mum]
    parent = zeros(1, params.ensSize*2);
    parent(1:params.ensSize) = params.gen.ens(mum).compo(1:params.ensSize);
    
    % Iterate through params.gen.ens[dad].compo(1:ensSize)
    % set parent[ensSize] to compo[e] any time a dad compo[e] has NO
    % matches to any parent[f] element
    for e = 1:params.ensSize
        compo = params.gen.ens(dad).compo;
        flag = 0;
        
        for f = 1:params.ensSize
            if parent(f) == compo(e)
                flag = flag + 1;
            end
        end
        
        if flag == 0
            parent(p) = compo(e);
            p = p + 1;
        end
    end
    
    %% Child
    
    parentLength = p - 1;
    indexes = randperm(parentLength, params.ensSize);
    params.gen.ens(k+where).compo = sort(parent(indexes));
end
